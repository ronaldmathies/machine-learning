"use strict";

let oDefaultConfig = {
  defaults: {
    neurons: 0,
    af: sigmoid,
    lr: 0.1
  },

  get: function(target, name) {
    return target.hasOwnProperty(name) ? target[name] : oDefaultConfig.defaults[name];
  }
};

class OutputLayer extends Layer {

  constructor(config) {
    super("OL");

    let parameters = new Proxy(config == null ? {} : config, oDefaultConfig);

    this.neurons = Assert.verifyNotNullOr0("neurons", this.parameters.neurons);
    this.af = Assert.verifyNotNull("af", this.parameters.af);
    this.lr = Assert.verifyNotNullOr0("lr", this.parameters.lr);

    this.values = null;
    this.values_activated = null;
    this.errors = null;
  }

  compile(previous_layer) {
    super.compile();

    this.previous_layer = Assert.verifyNotNull("previous_layer", previous_layer);

    this.bias = new Matrix(this.neurons, 1).randomize();
    this.weights = new Matrix(this.neurons, this.previous_layer.neurons).randomize();
  }

  feedForward(training = false) {
    super.feedForward();

    this.values = Matrix.multiplyDotProduct(this.weights, this.previous_layer.getValuesForFeedForward())
      .addMatrix(this.bias);
    this.values_activated = Matrix.map(this.values, this.af.func);
  }

  train(targets) {
    // First calculate the output error based on the (activated) values random
    // the target we were suppose to reach.
    let values = this.af.useOriginalValues ? this.values : this.values_activated;
    this.errors = Matrix.subtractMatrix(targets, values);

    // Calculate the gradient so we now how much we need to adjust our weights.
    // dfunc(values) * error * learning_rate
    let gradients = Matrix.map(values, this.af.dfunc)
      .multiplyWithMatrix(this.errors)
      .multiply(this.lr);

    // Calculate the hidden to output deltas
    // gradients * hidden_valuesT
    let weights_deltas = Matrix.multiplyDotProduct(gradients, Matrix.transpose(this.previous_layer.getValuesForDeltaCalculation()));

    // Adjust the hidden -> output weights with the deltas
    this.weights.addMatrix(weights_deltas);

    // Adjust the output bias.
    this.bias.addMatrix(gradients);
  }

  getWeights() {
    return this.weights;
  }
}
