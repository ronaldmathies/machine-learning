"use strict";

class Layer {

  constructor(label) {
    this.label = Assert.verifyNotNull("label", label);
  }

  compile(next_layer) {
	  // console.log("{}: Compiling layer", this.label)
  }

  feedForward() {
	  // console.log("{}: Feed forward", this.label)
  }

  train(inputs, targets) {
  }

}
