"use strict";

// filters: Integer, the dimensionality of the output
//          space (i.e. the number of output filters in the convolution).
// kernel:  Two integers, specifying the width and height of
//          the 2D convolution window.
// strides: An integer or tuple/list of 2 integers, specifying the strides of
//          the convolution along the width and height.
// padding: one of "valid" or "same"
let clDefaultConfig = {
  defaults: {
    filters: 1,
    kernel: {
      r: 2,
      c: 2
    },
    input: {
      r: 0,
      c: 0
    },
    stride: {
      r: 1,
      c: 1
    },
    padding: "valid"
  },

  get: function(target, name) {
    return target.hasOwnProperty(name) ? target[name] : clDefaultConfig.defaults[name];
  }
};

/*
 * More documentation about convolutional layers can be found at:
 *
 *
 * FEATURES TO IMPLEMENT:
 *
 * 0. Backpropagation...
 * 1. Handle multiple input arrays (channels?)
 * 4. Activation function?
 * 5. Learning rate (hyper parameter)
 *
 * Feed forward:
 * https://ujjwalkarn.me/2016/08/11/intuitive-explanation-convnets/
 * https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/convolution.html
 *
 * Back propagation:
 * https://medium.com/@2017csm1006/forward-and-backpropagation-in-convolutional-neural-network-4dfa96d7b37e
 * https://becominghuman.ai/only-numpy-implementing-convolutional-neural-network-using-numpy-deriving-forward-feed-and-back-458a5250d6e4
 *
 * Visualization of convolutional / pooling layers:
 * http://scs.ryerson.ca/~aharley/vis/conv/flat.html
 */
class ConvolutionalLayer extends Layer {

  constructor(config) {
    super("CL");

    let parameters = new Proxy(config == null ? {} : config, clDefaultConfig);

    this.filters = Assert.verifyIsNumberAndGreaterThen("filters", parameters.filters, 0);
    this.padding = Assert.verifyNotNullAndIsAnyOf("padding", parameters.padding, ["valid", "same"]);

    Assert.verifyIsNumberAndGreaterThen("kernel.r", parameters.kernel.r, 1);
    Assert.verifyIsNumberAndGreaterThen("kernel.c", parameters.kernel.c, 1);
    this.kernel = parameters.kernel;

    Assert.verifyIsNumberAndGreaterThen("stride.r", parameters.stride.r, 0);
    Assert.verifyIsNumberAndGreaterThen("stride.c", parameters.stride.c, 0);
    this.stride = parameters.stride;

    Assert.verifyIsNumberAndGreaterThen("input.r", parameters.input.r, 1);
    Assert.verifyIsNumberAndGreaterThen("input.c", parameters.input.c, 1);
    this.input = parameters.input;
  }

  compile(previous_layer, next_layer) {
    super.compile();

    this.previous_layer = Assert.verifyNotNull("previous_layer", previous_layer);
    this.next_layer = Assert.verifyNotNull("next_layer", next_layer);

    this.values = this.createOutputValues();
    this.weights = Array(this.filters).fill().map(w => new Matrix(this.kernel.r, this.kernel.c).randomize());
  }

  feedForward(training = false) {
    super.feedForward();

    let kernels = this.weights.map(w => Matrix.flip(w));
    let matrix = this.padMatrixIfApplicable(this.previous_layer.getValuesForFeedForward());

    this.values.forEach((value, v_idx) => {
      value.loop((_, i, j) => {
        let matrix_r = i * this.stride;
        let matrix_c = j * this.stride;

        let perceptive_field =
          Matrix.submatrix(matrix, matrix_r, this.pool_size, matrix_c, this.pool_size);

        let sum = 0;
        kernels[v_idx].loop((k_v, k_i, k_j) => {
          sum += perceptive_field.data[k_i][k_j] * k_v;
        });

        this.values[i][j] = sum;
      });
    });

    this.next_layer.feedForward(training);
  }

  padMatrixIfApplicable(matrix) {
    if (this.padding === 'same') {
      let padding = this.calculatePadding();
      return Matrix.pad(matrix, this.input.r + (2 * padding.r), this.input.c + (2 * padding.c))
    }

    return matrix;
  }

  createOutputValues() {
    let padding = this.calculatePadding();

    let value_rows = ((this.input.r - this.kernel.r + (2 * padding.r)) / this.stride.r) + 1;
    if (value_rows % 1 != 0) {
      throw "Combination of input.r, kernel.r size and stride.r doesn't add up to an integer for the row ( W - K + ( 2 * P ) / S) + 1 = " + value_rows;
    }

    let value_cols = ((this.input.c - this.kernel.c + (2 * padding.c)) / this.stride.c) + 1;
    if (value_cols % 1 != 0) {
      throw "Combination of input.c, kernel.c size and stride.c doesn't add up to an integer for the col ( W - K + ( 2 * P ) / S) + 1" + value_cols;
    }

    return Array(this.filters).fill().map(v => new Matrix(value_rows, value_cols));
  }

  calculatePadding() {
    return {r: (this.kernel.r - this.stride.r) / 2, c: (this.kernel.c - this.stride.c) / 2};
  }

  train(targets) {
    this.next_layer.train(targets);
  }

  getValuesForDeltaCalculation() {
    return this.values;
  }

  getValuesForFeedForward() {
    return this.values;
  }

}
