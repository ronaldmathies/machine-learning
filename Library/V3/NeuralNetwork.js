"use strict";

class NeuralNetwork {

  constructor(layers) {
    this.layers = layers;

    this.setLearningRate();
    this.setActivationFunction();
  }

  compile() {
    this.layers.forEach((layer, index) => {
      let is_first_layer = index == 0;
      let is_last_layer = index == this.layers.length;

      if (is_first_layer) {
        layer.compile(this.layers[index + 1]);
      } else if (is_last_layer) {
        layer.compile(this.layers[index - 1]);
      } else {
        layer.compile(this.layers[index - 1], this.layers[index + 1]);
      }

    });
  }

  setLearningRate(lr = 0.1) {
    this.lr = Assert.verifyNotNullOr0("lr", lr);
  }

  setActivationFunction(af = sigmoid) {
    this.af = af;
  }

  predict(inputs_array) {
    let inputs = Matrix.fromArray(inputs_array);
    this.layers[0].feedForward(inputs);

    return Matrix.toArray(this.layers.slice(-1)[0].values_activated);
  }

  train(inputs_array, targets_array) {
    let inputs = Matrix.fromArray(inputs_array);
    let targets = Matrix.fromArray(targets_array);

    this.layers[0].train(inputs, targets);
  }

}
