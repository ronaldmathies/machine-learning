"use strict";

let iDefaultConfig = {
  defaults: {
    neurons: 0
  },

  get: function(target, name) {
    return target.hasOwnProperty(name) ? target[name] : iDefaultConfig.defaults[name];
  }
};

class InputLayer extends Layer {

  constructor(config) {
    super("IL");

    let parameters = new Proxy(config == null ? {} : config, iDefaultConfig);

    this.neurons = Assert.verifyNotNullOr0("neurons", parameters.neurons);

    this.values = null;
  }

  compile(next_layer) {
    super.compile();

    this.next_layer = Assert.verifyNotNull("next_layer", next_layer);
  }

  feedForward(inputs, training = false) {
    super.feedForward();

    this.values = Assert.verifyNotNull("inputs", inputs);

    this.next_layer.feedForward(training);
  }

  train(inputs, targets) {
    // First perform the feedForward until the end, then start taining.
    this.feedForward(inputs, true);

    // Then train the next layer first.
    this.next_layer.train(Assert.verifyNotNullAndOfType("targets", targets, Matrix));

    // We don't have to do any training ourselfs since we don't have
    // any weights or biases.
  }

  getValuesForDeltaCalculation() {
    return this.values;
  }

  getValuesForFeedForward() {
    return this.values;
  }

}
