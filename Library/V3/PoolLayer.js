"use strict";

class PoolFunction {

  constructor(func, dfunc) {
    this.func = func;
    this.dfunc = dfunc;
  }

}

let maxPool = new PoolFunction(
  (matrix) => {
    return matrix.max();
  },
  (matrix) => {
    return matrix.max();
  }
);

let minPool = new PoolFunction(
  (matrix) => {
    return matrix.min();
  },
  (matrix) => {
    return matrix.min();
  }
);

let avgPool = new PoolFunction(
  (matrix) => {
    return matrix.avg();
  },
  () => {
    throw ("Average pool derivative function not implemented yet.");
  }
);

let pDefaultConfig = {
  defaults: {
    pool_size: 0,
    pool_func: maxPool,
    stride: 0,
    padding: "valid"
  },

  get: function(target, name) {
    return target.hasOwnProperty(name) ? target[name] : pDefaultConfig.defaults[name];
  }
};

class PoolLayer extends Layer {

  constructor(config) {
    super("PL");

    let parameters = new Proxy(config == null ? {} : config, pDefaultConfig);

    this.pool_size = Assert.verifyIsNumberAndGreaterThen("pool_size", parameters.pool_size, 0);
    this.pool_func = Assert.verifyNotNullAndOfType("pool_func", parameters.pool_func, PoolFunction);
    this.padding = Assert.verifyNotNullAndIsAnyOf("padding", parameters.padding, ["valid", "same"]);
    this.stride = Assert.verifyIsNumber("stride", parameters.stride);

    if (this.stride == 0) {
      this.stride = this.pool_size;
    }


  }

  compile(previous_layer, next_layer) {
    super.compile();

    this.previous_layer = Assert.verifyNotNull("previous_layer", previous_layer);
    this.next_layer = Assert.verifyNotNull("next_layer ", next_layer);

    this.values = new Array();
    this.matrix_pool = new Array();
  }

  feedForward(training = false) {
    super.feedForward();

    let matrices = this.previous_layer.getValuesForFeedForward();
    matrices.forEach((matrix) => {
      let output_r = Math.ceil(matrix.cols / this.stride);
      let output_c = Math.ceil(matrix.rows / this.stride);
      this.values.push(new Matrix(output_r, output_c));
    });

    if (training) {
      matrices.forEach((matrix) => {
        this.matrix_pool.push(new Matrix(matrix.rows, matrix.cols));
      });
    }

    this.values.forEach((value, v_idx) => {
      value.loop((_, i, j) => {
        let matrix_r = i * this.stride;
        let matrix_c = j * this.stride;

        let perceptive_field = Matrix.submatrix(matrices[v_idx], matrix_r, this.pool_size, matrix_c, this.pool_size);
        let pool_result = this.pool_func.func(perceptive_field);
        this.value.data[i][j] = pool_result.v;

        if (training) {
          this.matrix_pool[v_idx][pool_result.i][pool_result.j] = 1;
        }
      });
    });

    if (this.padding === 'same') {
      this.values.forEach((value, v_idx) => {
        this.values[v_idx] = Matrix.pad(value, matrix.rows, matrix.cols);
      });
    }

    this.next_layer.feedForward(training);
  }

  train(targets) {
    this.next_layer.train(targets);

    // nothing to do here, feedForward already calculated all relevent values.
  }

  getValuesForDeltaCalculation() {
    return this.values;
  }

  getValuesForFeedForward() {
    return this.values;
  }

  getWeights() {
    return this.matrix_pool;
  }
}
