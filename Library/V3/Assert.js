"use strict";

class Assert {

  static verifyNotNullAndIsAnyOf(name, value, valid_values) {
    Assert.verifyNotNull(name, value);

    if (valid_values.indexOf(value) == -1) {
      throw ("The field " + name + " should be any of the following values " + valid_values);
    }

    return value;
  }

  static verifyNotNullAndIsFunction(name, fn) {
    Assert.verifyNotNull(name, fn);
    if (!(fn && {}.toString.call(fn) === '[object Function]')) {
      throw ("The field " + name + " is not a function.");
    }

    return fn;
  }

  static verifyNotNullAndIsAnyOfType(name, value, valid_types) {
		Assert.verifyNotNull(name, value);

    let isValid = false;
    for (let key in valid_types) {
		  if (value instanceof valid_types[key]) {
        isValid = true;
		  }
    }

    if (!isValid) {
      throw ("The field " + name + " should be any of the following types " + valid_types.map((type) => type.name));
    }

		return value;
  }

  static verifyNotNullAndOfType(name, value, type) {
		Assert.verifyNotNull(name, value);
		if (!(value instanceof type)) {
			throw ("The field " + name + " is not of type " + type.name());
		}

		return value;
  }

  static verifyIsNumberAndGreaterThen(name, value, min_value) {
		Assert.verifyIsNumber(name, value);
		if (value <= min_value) {
			throw ("The field " + name + " should be greater then " + min_value + ".");
		}

		return value;
  }

  static verifyIsNumberAndGreaterThenOrEqualTo(name, value, min_value) {
		Assert.verifyIsNumber(name, value);
		if (value < min_value) {
			throw ("The field " + name + " should be greater then " + min_value + ".");
		}

		return value;
  }

  static verifyIsNumberAndSmallerThenOrEqualTo(name, value, min_value) {
		Assert.verifyIsNumber(name, value);
		if (value > max_value) {
			throw ("The field " + name + " should be smaller then " + min_value + ".");
		}

		return value;
  }

	static verifyIsNumber(name, value) {
		if (isNaN(parseFloat(value)) && isNaN(value - 0)) {
			throw ("The field " + name + " is not a number");
		}

		return value;
	}

  static verifyNotNullOr0(name, value) {
    Assert.verifyNotNull(name, value);

    if (value == 0) {
      throw ("The field " + name + " must not be 0.");
    }

    return value;
  }

  static verifyNotNull(name, value) {
    if (value == null) {
      throw ("The field " + name + " must not be null.");
    }

    return value;
  }

}
