// More information:
// https://docs.gimp.org/en/plug-in-convmatrix.html

let kfSharpen = Matrix.fromArray([
  [0, -1, 0],
  [-1, 5, -1],
  [0, -1, 0]
]);

let kfBlur = Matrix.fromArray([
  [1, 1, 1],
  [1, 1, 1],
  [1, 1, 1]
]);

let kfEdgeEnhance = Matrix.fromArray([
  [0, 0, 0],
  [-1, 1, 0],
  [0, 0, 0]
]);

let kfEdgeDetect = Matrix.fromArray([
  [0, 1, 0],
  [1, -4, 1],
  [0, 1, 0]
]);

let kfEmboss = Matrix.fromArray([
  [-2, -1, 0],
  [-1, 1, 1],
  [0, 1, 2]
]);
