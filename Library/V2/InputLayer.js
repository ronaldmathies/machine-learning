"use strict";

class InputLayer extends Layer {

  constructor(neurons) {
    super("IL");

    this.neurons = Assert.verifyNotNullOr0("neurons", neurons);

    this.values = null;
  }

  compile(next_layer) {
    super.compile();

    this.next_layer = Assert.verifyNotNull("next_layer", next_layer);
  }

  feedForward(inputs, training = false) {
    super.feedForward();

    this.values = Assert.verifyNotNullAndOfType("inputs", inputs, Matrix);

    this.next_layer.feedForward(training);
  }

  train(inputs, targets) {
    // First perform the feedForward until the end, then start taining.
    this.feedForward(inputs, true);

    // Then train the next layer first.
    this.next_layer.train(Assert.verifyNotNullAndOfType("targets", targets, Matrix));

    // We don't have to do any training ourselfs since we don't have
    // any weights or biases.
  }

  getValuesForDeltaCalculation() {
    return this.values;
  }

  getValuesForFeedForward() {
    return this.values;
  }

}
