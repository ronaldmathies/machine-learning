"use strict";

class HiddenLayer extends Layer {

  constructor(index, neurons, af, lr) {
    super("HL" + index);

    this.neurons = Assert.verifyNotNullOr0("neurons", neurons);
    this.af = Assert.verifyNotNull("af", af);
    this.lr = Assert.verifyNotNullOr0("lr", lr);

    this.values = null;
    this.values_activated = null;
    this.errors = null;
  }

  compile(previous_layer, next_layer) {
    super.compile();

    this.previous_layer = Assert.verifyNotNull("previous_layer", previous_layer);
    this.next_layer = Assert.verifyNotNull("next_layer ", next_layer);

    this.bias = new Matrix(this.neurons, 1).randomize("seed1");

    this.values = null;
    this.values_activated = null;
    this.weights = new Matrix(this.neurons, this.previous_layer.neurons).randomize("seed2");
  }

  feedForward(training = false) {
    super.feedForward();
    this.values = Matrix.multiplyDotProduct(this.weights, this.previous_layer.getValuesForFeedForward())
      .addMatrix(this.bias);
    this.values_activated = Matrix.map(this.values, this.af.func);

    this.next_layer.feedForward(training);
  }

  train(targets) {
    // Then train the next layer first.
    this.next_layer.train(targets);

    // Calculate the hidden errors
    this.errors = Matrix.multiplyDotProduct(Matrix.transpose(this.next_layer.getWeights()), this.next_layer.errors);

    // Calculate the gradient.
    let values = this.af.useOriginalValues ? this.values : this.values_activated;
    let gradients = Matrix.map(values, this.af.dfunc)
      .multiplyWithMatrix(this.errors)
      .multiply(this.lr);

    // Calculate input -> hidden deltas
    // hidden_gradients * inputsT
    let weights_deltas = Matrix.multiplyDotProduct(gradients, Matrix.transpose(this.previous_layer.getValuesForDeltaCalculation()));

    // Adjust the input -> hidden deltas
    this.weights.addMatrix(weights_deltas);

    // Adjust the hidden bias.
    this.bias.addMatrix(gradients);
  }

  getValuesForDeltaCalculation() {
    return this.values_activated;
  }

  getValuesForFeedForward() {
    return this.values_activated;
  }

  getWeights() {
    return this.weights;
  }
}
