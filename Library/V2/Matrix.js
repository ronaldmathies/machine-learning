"use strict";

class Matrix {

  /**
   * Constructs a new matrix with the given dimensions.
   */
  constructor(rows, cols) {
    this.rows = Assert.verifyIsNumber("rows", rows);
    this.cols = Assert.verifyIsNumber("cols", cols);

    this.data = Array(this.rows).fill().map(() => Array(this.cols).fill(0));
  }

  randomize(seed) {
    let generator = new Math.seedrandom(seed);

    this.map(v => generator() * 2 - 1);
    return this;
  }

  map(fn) {
    Assert.verifyNotNullAndIsFunction("fn", fn);
    for (let i = 0; i < this.rows; i++) {
      for (let j = 0; j < this.cols; j++) {
        this.data[i][j] = fn(this.data[i][j], i, j);
      }
    }
    return this;
  }

  loop(fn) {
    Assert.verifyNotNullAndIsFunction("fn", fn);
    for (let i = 0; i < this.rows; i++) {
      for (let j = 0; j < this.cols; j++) {
        fn(this.data[i][j], i, j);
      }
    }
    return this;
  }

  loopStep(r_step, c_step, fn) {
    Assert.verifyIsNumberAnGreaterThen("r_step", r_step, 0);
    Assert.verifyIsNumberAnGreaterThen("c_step", c_step, 0);
    Assert.verifyNotNullAndIsFunction("fn", fn);
    for (let i = 0; i < this.rows; i += r_step) {
      for (let j = 0; j < this.cols; j += c_step) {
        fn(this.data[i][j], i, j);
      }
    }
    return this;
  }

  loopRange(r_start, r_length, c_start, c_length, fn) {
    Assert.verifyIsNumberAnGreaterThenOrEqualTo("r_start", r_start, 0);
    Assert.verifyIsNumberAnGreaterThenOrEqualTo("r_length", r_length, 1);
    Assert.verifyIsNumberAnGreaterThenOrEqualTo("c_start", r_start, 0);
    Assert.verifyIsNumberAnGreaterThenOrEqualTo("c_length", c_length, 1);
    Assert.verifyIsNumberAndSmallerThenOrEqualTo("r_start and r_length combined ", r_start + r_length, this.rows)
    Assert.verifyIsNumberAndSmallerThenOrEqualTo("c_start and c_length combined ", c_start + c_length, this.cols)
    Assert.verifyNotNullAndIsFunction("fn", fn);

    for (let i = r_start; i < r_start + r_length; i++) {
      for (let j = c_start; j < c_start + c_length; j++) {
        fn(this.data[i][j], i, j);
      }
    }
    return this;
  }

  max() {
    let result = {
      v: this.data[0][0],
      i: 0,
      j: 0
    };
    this.loop((v, i, j) => {
      if (Math.max(result.v, v) == v) {
        result.v = v;
        result.i = i;
        result.j = j;
      }
    });

    return result;
  }

  min() {
    let result = {
      v: this.data[0][0],
      i: 0,
      j: 0
    };
    this.loop((v, i, j) => {
      if (Math.min(result.v, v) == v) {
        result.v = v;
        result.i = i;
        result.j = j;
      }
    });

    return result;
  }

  avg() {
    let result = 0;
    this.loopAll((v) => {
      result += v;
    });

    return result / (this.cols + this.rows);
  }

  sum() {
    let sum = 0;
    this.loop((v, i, j) => {
      sum += v;
    });
    return sum;
  }

  add(value_to_add) {
    Assert.verifyIsNumberAnGreaterThen("value_to_add", valueToAdd, 0);
    return this.map((v, i, j) => v + value_to_add);
  }

  addMatrix(matrix_to_add) {
    Assert.verifyNotNullAndOfType("matrix_to_add", matrix_to_add, Matrix);
    if (this.rows !== matrix_to_add.rows || this.cols !== matrix_to_add.cols) {
      throw ('Columns and rows of this matrix do not match those of the matrix to add.');
      return;
    }

    return this.map((v, i, j) => v + matrix_to_add.data[i][j]);
  }

  subtract(value_to_subtract) {
    Assert.verifyIsNumberAndGreaterThen("value_to_subtract", value_to_subtract, 0)
    return this.map(v => v - value_to_subtract);
  }

  subtractMatrix(matrix_to_subtract) {
    Assert.verifyNotNullAndOfType("matrix_to_subtract", matrix_to_subtract, Matrix);
    if (this.rows !== matrix_to_subtract.rows || this.cols !== matrix_to_subtract.cols) {
      throw ('Columns and rows of this matrix do not match those of the matrix to subtract.');
      return;
    }

    return this.map((v, i, j) => v - matrix_to_subtract.data[i][j]);
  }

  multiply(other) {
    Assert.verifyIsNumberAndGreaterThen("other", other, 0)

    return this.map(v => v * other);
  }

  multiplyWithMatrix(matrix) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    if (this.rows !== matrix.rows || this.cols !== matrix.cols) {
      throw ('Columns and rows of this matrix do not match those of the matrix to multiply with.');
      return;
    }

    return this.map((v, i, j) => this.data[i][j] * matrix.data[i][j]);
  }

  static add(matrix, value_to_add) {
    return Matrix.clone(matrix).add(value_to_add);
  }

  static addMatrix(matrix, matrix_to_add) {
    return Matrix.clone(matrix).addMatrix(matrix_to_add);
  }

  static subtract(matrix, value_to_subtract) {
    return Matrix.clone(matrix).subtract(value_to_subtract);
  }

  static subtractMatrix(matrix, matrix_to_subtract) {
    return Matrix.clone(matrix).subtractMatrix(matrix_to_subtract);
  }

  static multiplyDotProduct(matrix, with_matrix) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    Assert.verifyNotNullAndOfType("with_matrix", with_matrix, Matrix);

    if (matrix.cols !== with_matrix.rows) {
      throw ('Columns and rows of matrix do not math those of with_matrix.')
      return;
    }

    return new Matrix(matrix.rows, with_matrix.cols)
      .map((e, i, j) => {
        // Dot product of values in col
        let sum = 0;
        for (let k = 0; k < matrix.cols; k++) {
          sum += matrix.data[i][k] * with_matrix.data[k][j];
        }
        return sum;
      });
  }

  static multiply(matrix, value) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    return Matrix.clone(matrix).multiply(value);
  }

  static multiplyWithMatrix(matrix, with_matrix) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    return Matrix.clone(matrix).multiplyWithMatrix(withMatrix);
  }

  static transpose(matrix) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    return new Matrix(matrix.cols, matrix.rows).map((_, i, j) => matrix.data[j][i]);
  }

  static flip(matrix) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    return new Matrix(matrix.cols, matrix.rows).map(
      (_, i, j) => matrix.data[matrix.rows - 1 - i][matrix.cols - 1 - j]);
  }

  // Kernel is sometimes also refered to as filter or feature detector.
  // Don't forget to perform a non-linear function over the result (ReLu)
  // More information:
  // https://ujjwalkarn.me/2016/08/11/intuitive-explanation-convnets/
  // https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/convolution.html
  static convolve(matrix, weights, padding = "valid", stride = {
    r: 1,
    c: 1
  }, filters = 1) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    Assert.verifyNotNullAndIsAnyOf("padding", padding, ["valid", "same"]);
    let kernel = {
      r: weights[0].rows,
      c: weights[0].cols
    }

    let kernels = [];
    weights.forEach(w => kernels.push(Matrix.flip(w)));

    let row_padding = 0;
    let col_padding = 0;
    let prepared_matrix = matrix;
    if (padding === 'same') {
      row_padding = (kernel.r - stride.r) / 2;
      col_padding = (kernel.c - stride.c) / 2;
      prepared_matrix = Matrix.pad(matrix, matrix.rows + (2 * row_padding), matrix.cols + (2 * col_padding))
    }

    let value_rows = ((matrix.rows - kernel.r + (2 * row_padding)) / stride.r) + 1;
    if (value_rows % 1 != 0) {
      throw "Combination of input size, kernel.r size and stride.r doesn't add up to an integer for the row ( W - K + ( 2 * P ) / S) + 1 = " + value_rows;
    }

    let value_cols = ((matrix.cols - kernel.c + (2 * col_padding)) / stride.c) + 1;
    if (value_cols % 1 != 0) {
      throw "Combination of input size, kernel.c size and stride.c doesn't add up to an integer for the col ( W - K + ( 2 * P ) / S) + 1" + value_cols;
    }

    values = Array(filters).fill().map(v => new Matrix(value_rows, value_cols));
    values[0].loop((_, i, j) => {
      let sum = Array(filters).fill().map(v => 0);
      kernels[0].loop((_, k_i, k_j) => {
        let pm_row = i * stride.r;
        let pm_col = j * stride.c;

        kernels.forEach((kernel, k_idx) => {
          sum[k_idx] += prepared_matrix.data[pm_row + k_i][pm_col + k_j] * kernel.data[k_i][k_j];
        });
      });

      sum.forEach((v, s_idx) => values[s_idx].data[i][j] = v);
    });

    return values;
  }

  static pad(matrix, rows, cols) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    Assert.verifyIsNumberAndGreaterThenOrEqualTo("rows", rows, matrix.rows)
    Assert.verifyIsNumberAndGreaterThenOrEqualTo("cols", cols, matrix.cols)

    let result = new Matrix(rows, cols);

    let startx = Math.floor((cols - matrix.cols) / 2);
    let starty = Math.floor((rows - matrix.rows) / 2);

    matrix.loop((v, i, j) => {
      result.data[starty + i][startx + j] = v;
    });

    return result;
  }

  static submatrix(matrix, r_start, r_length, c_start, c_length) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    Assert.verifyIsNumberAndSmallerThenOrEqualTo("r_start and r_length combined ", r_start + r_length, matrix.rows)
    Assert.verifyIsNumberAndSmallerThenOrEqualTo("c_start and c_length combined ", c_start + c_length, matrix.cols)

    let c_length_b = Math.min(matrix.cols - c_start, c_length);
    let r_Lenght_b = Math.min(matrix.rows - r_start, r_length);

    let result = new Matrix(rLength, cLength);
    matrix.loopRange(r_start, r_Lenght_b, c_start, c_length_b, (v, i, j) => {
      result.data[i - r_start][j - c_start] = v;
    });
    return result;
  }

  static fromArray(array) {
    if (Array.isArray(array[0])) {
      return new Matrix(array.length, array[0].length).map((v, i, j) => array[i][j]);
    } else {
      return new Matrix(array.length, 1).map((v, i, j) => array[i]);
    }
  }

  static toArray(matrix) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    let array = [];
    for (let i = 0; i < matrix.rows; i++) {
      for (let j = 0; j < matrix.cols; j++) {
        array.push(matrix.data[i][j]);
      }
    }
    return array;
  }

  static map(matrix, fn) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    Assert.verifyNotNullAndIsFunction("fn", fn);
    return new Matrix(matrix.rows, matrix.cols).map((v, i, j) => fn(matrix.data[i][j], i, j));
  }

  static clone(matrix) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    return new Matrix(matrix.rows, matrix.cols).addMatrix(matrix);
  }

  static serialize(matrix) {
    Assert.verifyNotNullAndOfType("matrix", matrix, Matrix);
    return JSON.stringify(matrix);
  }

  static deserialize(json) {
    let data = JSON.parse(json);
    let matrix = new Matrix(data.rows, data.cols);
    matrix.data = data.data;
    return matrix;
  }

  log() {
    console.table(this.data);
    return this;
  }
}
