"use strict";

class NeuralNetwork {

  constructor(input_layer_neurons, hidden_layers_neurons, output_layer_neurons) {
    this.input_layer_neurons = Assert.verifyNotNullOr0("input_layer_neurons", input_layer_neurons);
    this.hidden_layers_neurons = Assert.verifyNotNullOr0("hidden_layers_neurons", hidden_layers_neurons);
    this.output_layer_neurons = Assert.verifyNotNullOr0("output_layer_neurons", output_layer_neurons);

    this.setLearningRate();
    this.setActivationFunction();
  }

  compile() {
    // Create all the layers
    this.input_layer = new InputLayer(this.input_layer_neurons);

    // Create all the hidden layers
    this.hidden_layers = Array();
    this.hidden_layers_neurons.forEach((hidden_neurons, index) => this.hidden_layers
      .push(new HiddenLayer(index, hidden_neurons, this.af, this.lr)));

    // Create the output layer
    this.output_layer = new OutputLayer(this.output_layer_neurons, this.af, this.lr);

    // Compile the input layer
    this.input_layer.compile(this.hidden_layers[0]);

    // Compile the hidden layers
    let previous_layer = this.input_layer;
    this.hidden_layers.forEach((hidden_layer, index) => {
      let next_layer = index < this.hidden_layers.length - 1 ?
        this.hidden_layers[index + 1] : this.output_layer;
      hidden_layer.compile(previous_layer, next_layer);
      previous_layer = hidden_layer;
    });

    // Compile the output layer.
    this.output_layer.compile(previous_layer);
  }

  setLearningRate(lr = 0.1) {
    this.lr = Assert.verifyNotNullOr0("lr", lr);
  }

  setActivationFunction(af = sigmoid) {
    this.af = af;
  }

  predict(inputs_array) {
    let inputs = Matrix.fromArray(inputs_array);
    this.input_layer.feedForward(inputs);

    return Matrix.toArray(this.output_layer.values_activated);
  }

  train(inputs_array, targets_array) {
    let inputs = Matrix.fromArray(inputs_array);
    let targets = Matrix.fromArray(targets_array);

    this.input_layer.train(inputs, targets);
  }

}
