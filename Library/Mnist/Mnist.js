"use strict";

/**
 * Reads in the MNIST database
 * http://yann.lecun.com/exdb/mnist/
 */

function vanillaFilter(v) {
  return v;
}

function normalizedFilter(v) {
  return v / 255;
}

function roundedFilter(v) {
  return Math.round(v / 255);
}

const IMAGE_ID = 2051;
const LABEL_ID = 2049;

class Mnist {

  constructor(filter = normalizedFilter) {
    this.files = {
      train_images: 'http://localhost:8000/data/train-images-idx3-ubyte',
      train_labels: 'http://localhost:8000/data/train-labels-idx1-ubyte',
      test_images: 'http://localhost:8000/data/t10k-images-idx3-ubyte',
      test_labels: 'http://localhost:8000/data/t10k-labels-idx1-ubyte'
    };

    this.train_images = null;
    this.train_labels = null;
    this.test_images = null;
    this.test_labels = null;

    this.filter = filter;
  }

  loadTrainingData(complete) {
    return new Promise(async (complete) => {
      this.train_images = await this.loadImageData(this.files.train_images);
      this.train_labels = await this.loadLabelData(this.files.train_labels);
      this.test_images = await this.loadImageData(this.files.test_images);
      this.test_labels = await this.loadLabelData(this.files.test_labels);

      console.log("Loaded " + this.train_images.length + " train images.");
      console.log("Loaded " + this.test_images.length + " test images.");

      complete();
    });
  }

  async loadImageData(file) {
    let buffer = await fetch(file).then(r => r.arrayBuffer());

    // [offset] [type]          [value]          [description]
    // 0000     32 bit integer  0x00000803(2051) magic number
    // 0004     32 bit integer  60000            number of images
    // 0008     32 bit integer  28               number of rows
    // 0012     32 bit integer  28               number of columns
    // 0016     unsigned byte   ??               pixel
    // 0017     unsigned byte   ??               pixel
    // ........
    // xxxx     unsigned byte   ??               pixel
    let header_count = 4;
    let header_view = new DataView(buffer, 0, 4 * header_count);
    let headers = new Array(header_count).fill().map((_, i) => header_view.getUint32(4 * i, false));

    let fileId = headers[0];
    let numberOfImages = headers[1];
    let numberOfRows = headers[2];
    let numberOfColumns = headers[3];

    if (fileId != IMAGE_ID) {
      throw ("File " + file + " is not an image file.");
    }

    let dataLength = numberOfRows * numberOfColumns;

    // Retrieve all image data (from where the image data starts in the buffer)
    let imageDataOffset = header_count * 4;
    let data = new Uint8Array(buffer, imageDataOffset);

    let images = [];
    for (let i = 0; i < numberOfImages; i++) {
      // Retreive a single image from the buffer and convert it to a float32 array.
      let data32 = new Float32Array(data.subarray(dataLength * i, dataLength * (i + 1)));

      // Perform any filtering that has been set.
      images.push(data32.map(this.filter));
    }

    return images;
  }

  async loadLabelData(file) {
    let buffer = await fetch(file).then(r => r.arrayBuffer());

    // [offset] [type]          [value]          [description]
    // 0000     32 bit integer  0x00000801(2049) magic number (MSB first)
    // 0004     32 bit integer  60000            number of items
    // 0008     unsigned byte   ??               label
    // 0009     unsigned byte   ??               label
    // ........
    // xxxx     unsigned byte   ??               label
    let headerCount = 2;
    let headerView = new DataView(buffer, 0, 4 * headerCount);
    let headers = new Array(headerCount).fill().map((_, i) => headerView.getUint32(4 * i, false));

    let fileId = headers[0];
    let numberOfLabels = headers[1];

    if (fileId != LABEL_ID) {
      throw ("File " + file + " is not a label file.");
    }

    let labels = new Uint8Array(buffer, headerCount * 4);
    return labels;
  }


}
