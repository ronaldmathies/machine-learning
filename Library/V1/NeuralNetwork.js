"use strict";

class FeedForwardResult {

  constructor() {
    this.hidden_values = null;
    this.hidden_values_activated = null;
    this.output_values = null;
    this.output_values_activated = null;
  }

}

class NeuralNetwork {

  constructor(input_nodes, hidden_nodes, output_nodes) {
    this.input_nodes = input_nodes;
    this.hidden_nodes = hidden_nodes;
    this.output_nodes = output_nodes;

    this.weights_ih = new Matrix(this.hidden_nodes, this.input_nodes);
    this.weights_ho = new Matrix(this.output_nodes, this.hidden_nodes);

    this.weights_ih.randomize();
    this.weights_ho.randomize();

    this.hidden_bias = new Matrix(this.hidden_nodes, 1);
    this.output_bias = new Matrix(this.output_nodes, 1);

    this.hidden_bias.randomize();
    this.output_bias.randomize();

    // Set default learning reate and activiation function.
    this.setLearningRate();
    this.setActivationFunction(sigmoid);
  }

  setLearningRate(learning_rate = 0.1) {
    this.learning_rate = learning_rate;
  }

  setActivationFunction(activationFunction = sigmoid) {
    this.activationFunction = activationFunction;
  }

  predict(inputs_array) {
    let train_data = {
      inputs: Matrix.fromArray(inputs_array),
      targets: null
    };

    let ffResult = this.feedForward(train_data);

    return ffResult.output_values_activated.toArray();
  }

  feedForward(train_data) {
    let ffResult = new FeedForwardResult(this.activiationFunction);

    // Calculate the inputs -> hidden values
    // sigmoid(weights_ih * inputs_values + bias)
    ffResult.hidden_values = Matrix.multiplyDotProduct(this.weights_ih, train_data.inputs)
      .addMatrix(this.hidden_bias);
    ffResult.hidden_values_activated = Matrix.map(ffResult.hidden_values, this.activationFunction.func);

    // Calculate the hidden -> output values
    // sigmoid(weights_ho * hidden_values + bias)
    ffResult.output_values = Matrix.multiplyDotProduct(this.weights_ho, ffResult.hidden_values_activated)
      .addMatrix(this.output_bias);
    ffResult.output_values_activated = Matrix.map(ffResult.output_values, this.activationFunction.func);

    return ffResult;
  }

  train(inputs_array, targets_array) {
    let train_data = {
      inputs: Matrix.fromArray(inputs_array),
      targets: Matrix.fromArray(targets_array)
    };

    /*******************************************/
    /* Feed forward                            */
    /*******************************************/

    let ffResult = this.feedForward(train_data);

    /*******************************************/
    /* Calculate error                         */
    /*******************************************/

    // Calculate the output errors
    // target - output_values
    let output_values = this.activationFunction.useOriginalValues ?
      ffResult.output_values : ffResult.output_values_activated;
    let output_errors = Matrix.subtractMatrix(train_data.targets, output_values);

    /*******************************************/
    /* Adjust hidden -> output weights         */
    /*******************************************/

    // Calculate the hidden to output gradient
    // (output_values * (1 - output_values)) * output_errors * learning_rate
    let gradients = Matrix.map(output_values, this.activationFunction.dfunc)
      .multiplyWithMatrix(output_errors)
      .multiply(this.learning_rate);

    // Calculate the hidden to output deltas
    // gradients * hidden_valuesT
    let weights_ho_deltas = Matrix.multiplyDotProduct(gradients, Matrix.transpose(ffResult.hidden_values_activated));

    // Adjust the hidden -> output weights with the deltas
    this.weights_ho.addMatrix(weights_ho_deltas);

    // Adjust the output bias.
    this.output_bias.addMatrix(gradients);


    /*******************************************/
    /* Adjust input -> hidden weights          */
    /*******************************************/

    // Calculate the hidden errors
    let hidden_errors = Matrix.multiplyDotProduct(Matrix.transpose(this.weights_ho), output_errors);



    // Calculate the hidden gradient.
    let hidden_values = this.activationFunction.useOriginalValues ?
      ffResult.hidden_values : ffResult.hidden_values_activated;
    let hidden_gradients = Matrix.map(hidden_values, this.activationFunction.dfunc)
      .multiplyWithMatrix(hidden_errors)
      .multiply(this.learning_rate);

    // Calculate input -> hidden deltas
    // hidden_gradients * inputsT
    let weights_ih_deltas = Matrix.multiplyDotProduct(hidden_gradients, Matrix.transpose(train_data.inputs));

    // Adjust the input -> hidden deltas
    this.weights_ih.addMatrix(weights_ih_deltas);

    // Adjust the hidden bias.
    this.hidden_bias.addMatrix(hidden_gradients);
  }

}
