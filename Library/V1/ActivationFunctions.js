// function sigmoid(v) {
//   return 1 / (1 + Math.exp(-v));
// }
//
// function dsigmoid(v) {
//   return v * (1 - v);
// }
//
// function tanh(v) {
//   return v;
// }

class ActivationFunction {

  constructor(func, dfunc, useOriginalValues) {
    this.func = func;
    this.dfunc = dfunc;
    this.useOriginalValues = useOriginalValues;
  }

}

// Range (0, 1)
let sigmoid = new ActivationFunction(
  v => 1 / (1 + Math.exp(-v)),
  v => v * (1 - v),
  useOriginalValues = false
);

// Range (-1, 1)
let tanh = new ActivationFunction(
  v => Math.tanh(v),
  v => 1 - (v * v),
  useOriginalValues = false
);

// Range (-PI/2, PI/2)
let arctan = new ActivationFunction(
  v => Math.atan(v),
  v => 1 / ((v * v) + 1),
  useOriginalValues = true
);

// Range (-1, 1)
let softsign = new ActivationFunction(
    v => v / (1 + Math.abs(v)),
    v => 1 / Math.pow((Math.abs(v) + 1), 2),
    useOriginalValues = true
);

// Range [0, INFINITY)
let relu = new ActivationFunction(
    v => v < 0 ? 0 : v,
    v => v < 0 ? 0 : 1,
    useOriginalValues = true
);

// Range (-INFINITY, INFINITY)
let leaky_relu = new ActivationFunction(
  v => v < 0 ? 0.01 * v : v,
  v => v < 0 ? 0.01 : 1,
  useOriginalValues = false
);

// Range (0, INFINITY)
let softplus = new ActivationFunction(
    v => Math.log(1 + Math.exp(v)),
    v => 1 / (1 + Math.exp(-v)),
    useOriginalValues = true
);

// Range (0, 1]
let gaussian = new ActivationFunction(
    v => Math.exp(-1 * (v * v)),
    v => -2 * v * Math.exp(-1 * (v * v)),
    useOriginalValues = true
 );
