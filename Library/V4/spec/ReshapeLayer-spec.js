describe("ReshapeLayer Suite", function() {
  it("ReshapeLayer.config.input_shape.d Should be larger -1.", function() {
    expect(() => {
      new ReshapeLayer({
        input_shape: { d: -1, r: 1, c: 1 },
        target_shape: { d: 1, r: 1, c: 1}
      });
    }).toThrow("The field input_shape.d should be larger then -1.");
  });

  it("ReshapeLayer.config.input_shape.r Should be larger then 0.", function() {
    expect(() => {
      new ReshapeLayer({
        input_shape: { d: 1, r: 0, c: 1 },
        target_shape: { d: 1, r: 1, c: 1}
      });
    }).toThrow("The field input_shape.r should be larger then 0.");
  });

  it("ReshapeLayer.config.input_shape.c Should be larger then 0.", function() {
    expect(() => {
      new ReshapeLayer({
        input_shape: { d: 1, r: 1, c: 0 },
        target_shape: { d: 1, r: 1, c: 1}
      });
    }).toThrow("The field input_shape.c should be larger then 0.");
  });

  it("ReshapeLayer.config.input_shape.d Should be larger -1.", function() {
    expect(() => {
      new ReshapeLayer({
        input_shape: { d: -1, r: 1, c: 1 },
        target_shape: { d: 1, r: 1, c: 1}
      });
    }).toThrow("The field input_shape.d should be larger then -1.");
  });

  it("ReshapeLayer.config.target_shape.r Should be larger then 0.", function() {
    expect(() => {
      new ReshapeLayer({
        input_shape: { d: 0, r: 16, c: 16 },
        target_shape: { d: 0, r: 0, c: 1 }
      });
    }).toThrow("The field target_shape.r should be larger then 0.");
  });

  it("ReshapeLayer.config.target_shape.c Should be larger then 0.", function() {
    expect(() => {
      new ReshapeLayer({
        input_shape: { d: 0, r: 16, c: 16 },
        target_shape: { d: 0, r: 1, c: 0 }
      });
    }).toThrow("The field target_shape.c should be larger then 0.");
  });

  it("ReshapeLayer.next_layer Should not be null after compile.", function() {
    expect(() => {
      new ReshapeLayer({
        input_shape: { d: 0, r: 16, c: 16 },
        target_shape: { d: 0, r: 1, c: 1 }
      }).compile();
    }).toThrow("The field next_layer must not be null.");
  });

  it("ReshapeLayer.full integration (I=0x1x2).", function() {
    let layerMock = jasmine.createSpyObj("layerMock", {
      "feedForward": null,
      "train": [
        [2],
        [3]
      ]
    });
    let reshapeLayer = new ReshapeLayer({
      input_shape: { r: 1, c: 2 },
      target_shape: { r: 2, c: 1 }
    });
    reshapeLayer.compile(layerMock);

    reshapeLayer.feedForward(
      [
        [1, 2]
      ], false);

    expect(layerMock.feedForward).toHaveBeenCalledWith([
      [1],
      [2]
    ], false);

    reshapeLayer.train([]);

    expect(reshapeLayer.errors).toEqual(
      [
        [2, 3]
      ]
    );
  });

  it("ReshapeLayer.full integration (I=0x2x1).", function() {
    let layerMock = jasmine.createSpyObj("layerMock", {
      "feedForward": null,
      "train": [
        [4, 5, 6, 7]
      ]
    });
    let reshapeLayer = new ReshapeLayer({
      input_shape: { d: 2, r: 2, c: 1 },
      target_shape: { r: 1, c: 4 }
    });
    reshapeLayer.compile(layerMock);
    reshapeLayer.feedForward([
        [
          [1],
          [2],
        ],
        [
          [3],
          [4]
        ]
      ],
      false);

    expect(layerMock.feedForward).toHaveBeenCalledWith([
      [1, 2, 3, 4]
    ], false);

    reshapeLayer.train([], []);

    expect(reshapeLayer.errors).toEqual([
        [
          [4],
          [5],
        ],
        [
          [6],
          [7]
        ]
      ]);
  });
});
