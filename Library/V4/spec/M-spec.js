 describe("M Suite", function() {

   it("M.createArray Should create a 1D array initialized.", function() {
     expect(M.createArray({
       c: 3
     }, true)).toEqual([0, 0, 0]);
   });

   it("M.createArray Should create a 1D array uninitialized.", function() {
     expect(M.createArray({
       c: 3
     })).toEqual(new Array(3));
   });

   it("M.createArray Should create a 2d array initialized.", function() {
     expect(M.createArray({
       r: 3,
       c: 3
     }, true)).toEqual([
       [0, 0, 0],
       [0, 0, 0],
       [0, 0, 0]
     ]);
   });

   it("M.createArray Should create a 2d array uninitialized.", function() {
     expect(M.createArray({
       r: 3,
       c: 3
     })).toEqual([
       new Array(3),
       new Array(3),
       new Array(3)
     ]);
   });

   it("M.createArray Should create a 3d array initialized.", function() {
     expect(M.createArray({
       d: 1,
       r: 3,
       c: 3
     }, true)).toEqual([
       [
         [0, 0, 0],
         [0, 0, 0],
         [0, 0, 0]
       ]
     ]);
   });

   it("M.createArray Should create a 3d array uninitialized.", function() {
     expect(M.createArray({
       d: 1,
       r: 3,
       c: 3
     })).toEqual([
       [
         new Array(3),
         new Array(3),
         new Array(3)
       ]
     ]);
   });

   it("M.reshape Should reshape an input of 4x3 to 3x4.", function() {
     expect(M.reshape([
       [1, 2, 3],
       [1, 2, 3],
       [1, 2, 3],
       [1, 2, 3]
     ], {
       r: 3,
       c: 4
     })).toEqual(
       [
         [1, 2, 3, 1],
         [2, 3, 1, 2],
         [3, 1, 2, 3]
       ]
     );
   });

   it("M.reshape Should reshape an input of 3x4 to 4x3.", function() {
     expect(M.reshape([
       [1, 2, 3, 1],
       [2, 3, 1, 2],
       [3, 1, 2, 3]
     ], {
       r: 4,
       c: 3
     })).toEqual(
       [
         [1, 2, 3],
         [1, 2, 3],
         [1, 2, 3],
         [1, 2, 3]
       ]
     );
   });

   it("M.reshape Should reshape an input of 1x12 to 2x3x2.", function() {
     expect(M.reshape([
       [1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]
     ], {
       d: 2,
       r: 3,
       c: 2
     })).toEqual([
       [
         [1, 2],
         [3, 1],
         [2, 3]
       ],
       [
         [1, 2],
         [3, 1],
         [2, 3]
       ]
     ]);
   });

   it("M.reshape Should throw an error when the input does not fit the output (1x12 => 1x3x2)", function() {
     expect(() => {
       M.reshape([
         [1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3]
       ], {
         d: 1,
         r: 3,
         c: 2
       })
     }).toThrow("The total number of input digits will not fit the total number of output digits.");
   });

   // it("M.max Should result in highest number to be 3 in the 0x3 matrix.", function() {
   //   expect(M.max(
   //     [1, 3, 2]
   //   )).toEqual({
   //     v: 3,
   //     c: 1
   //   });
   // });
   //
   // it("M.max Should result in highest number to be 8 in a 3x3 matrix.", function() {
   //   expect(M.max(
   //     [
   //       [1, 1, 1],
   //       [1, 8, 1],
   //       [1, 1, 1]
   //     ]
   //   )).toEqual({
   //     v: 8,
   //     r: 1,
   //     c: 1
   //   });
   // });
   //
   // it("M.max Should result in highest number to be 8 in a 2x3x3 matrix.", function() {
   //   expect(M.max(
   //     [
   //       [
   //         [1, 1, 1],
   //         [1, 5, 1]
   //       ],
   //       [
   //         [1, 1, 1],
   //         [1, 8, 1]
   //       ]
   //     ]
   //   )).toEqual({
   //     v: 8,
   //     d: 1,
   //     r: 1,
   //     c: 1
   //   });
   // });

   it("M.min Should result in lowest number to be 1 in a 0x3 matrix.", function() {
     expect(M.min(
       [2, 8, 1]
     )).toEqual({
       v: 1,
       c: 2
     });
   });

   it("M.min Should result in lowest number to be 1 in a 3x3 matrix.", function() {
     expect(M.min(
       [
         [8, 8, 8],
         [8, 1, 8],
         [8, 8, 8]
       ]
     )).toEqual({
       v: 1,
       r: 1,
       c: 1
     });
   });

   it("M.max Should result in lowest number to be 8 in a 2x3x3 matrix.", function() {
     expect(M.min(
       [
         [
           [4, 6, 2],
           [5, 5, 7]
         ],
         [
           [4, 1, 2],
           [9, 8, 5]
         ]
       ]
     )).toEqual({
       v: 1,
       d: 1,
       r: 0,
       c: 1
     });
   });

   it("M.sum Should result in a sum of 4 in a 0x3 matrix.", function() {
     expect(M.sum(
       [1, 2, 1]
     )).toBe(4);
   });

   it("M.sum Should result in a sum of 13 in a 3x3 matrix.", function() {
     expect(M.sum(
       [
         [1, 2, 1],
         [2, 1, 2],
         [1, 2, 1]
       ]
     )).toBe(13);
   });

   it("M.sum Should result in a sum of 18 in a 2x2x3 matrix.", function() {
     expect(M.sum(
       [[
         [2, 1, 2],
         [1, 2, 1]
       ],
       [
         [1, 2, 1],
         [2, 1, 2],
       ]]
     )).toBe(18);
   });

   it("M.avg2D Should result in an average of 2.", function() {
     expect(M.avg2D(
       [
         [1, 2, 1],
         [5, 0, 5],
         [1, 2, 1]
       ]
     )).toBe(2);
   });

   it("M.add Should result in a 0x3 array where the input (0x3) was added with the 0x3 add array.", function() {
     expect(M.add(
       [1, 2, 1], [1, 2, 1]
     )).toEqual(
       [2, 4, 2]
     );
   });

   it("M.add Should result in a 0x3 array where the input (0x3) was added with 1 element wise.", function() {
     expect(M.add(
       [1, 2, 1], 1
     )).toEqual(
       [2, 3, 2]
     );
   });

   it("M.add Should result in a 3x3 array where the input (3x3) was added with the 3x3 add array.", function() {
     expect(M.add(
       [
         [1, 2, 1],
         [2, 1, 2],
         [1, 2, 1]
       ], [
         [1, 2, 1],
         [2, 1, 2],
         [1, 2, 1]
       ]
     )).toEqual(
       [
         [2, 4, 2],
         [4, 2, 4],
         [2, 4, 2]
       ]
     );
   });

   it("M.add Should result in a 2x3x3 array where the input (2x3x3) was added with the 2x3x3 add array.", function() {
     expect(M.add(
       [
         [
           [1, 2, 1],
           [2, 1, 2],
           [1, 2, 1]
         ],
         [
           [2, 1, 2],
           [1, 2, 1],
           [2, 1, 2]
         ]
       ], [
         [
           [1, 2, 1],
           [2, 1, 2],
           [1, 2, 1]
         ],
         [
           [2, 1, 2],
           [1, 2, 1],
           [2, 1, 2]
         ]
       ]
     )).toEqual(
       [
         [
           [2, 4, 2],
           [4, 2, 4],
           [2, 4, 2]
         ],
         [
           [4, 2, 4],
           [2, 4, 2],
           [4, 2, 4]
         ]
       ]
     );
   });

   it("M.add Should result in a 3x3 array where the input (3x3 array) was added with 1 element wise.", function() {
     expect(M.add(
       [
         [1, 2, 1],
         [2, 1, 2],
         [1, 2, 1]
       ],
       1
     )).toEqual(
       [
         [2, 3, 2],
         [3, 2, 3],
         [2, 3, 2]
       ]
     );
   });

   it("M.add Should result in a 2x3x3 array where the input (2x3x3 array) was added with 1 element wise.", function() {
     expect(M.add(
       [
         [
           [1, 2, 1],
           [2, 1, 2],
           [1, 2, 1]
         ],
         [
           [2, 1, 2],
           [1, 2, 1],
           [2, 1, 2]
         ]
       ],
       1
     )).toEqual(
       [
         [
           [2, 3, 2],
           [3, 2, 3],
           [2, 3, 2]
         ],
         [
           [3, 2, 3],
           [2, 3, 2],
           [3, 2, 3]
         ]
       ]
     );
   });

   it("M.subtract Should result in a 3x3 array where the input (3x3) was subtracted with the 3x3 subtract array.", function() {
     expect(M.subtract(
       [
         [1, 2, 1],
         [2, 1, 2],
         [1, 2, 1]
       ], [
         [0, 1, 0],
         [1, 0, 1],
         [0, 1, 0]
       ]
     )).toEqual(
       [
         [1, 1, 1],
         [1, 1, 1],
         [1, 1, 1]
       ]
     );
   });

   it("M.subtract Should result in a 3x3 array where the input (3x3 array) was substracted with 1 element wise.", function() {
     expect(M.subtract(
       [
         [1, 2, 1],
         [2, 1, 2],
         [1, 2, 1]
       ],
       1
     )).toEqual(
       [
         [0, 1, 0],
         [1, 0, 1],
         [0, 1, 0]
       ]
     );
   });

   it("M.subtract Should result in a 1x3 array where the input (1x3 array) was substracted with 1 element wise.", function() {
     expect(M.subtract(
       [1, 2, 1],
       1
     )).toEqual(
       [0, 1, 0]
     );
   });

   it("M.multiply Should result in a 1x3 array where the input (1x3 array) was multiplied with the 1x3 multiply array.", function() {
     expect(M.multiply(
       [1, 2, 1], [0, 1, 0]
     )).toEqual(
       [0, 2, 0]
     );
   });

   it("M.multiply Should result in a 3x3 array where the input (3x3 array) was multiplied with the 3x3 multiply array.", function() {
     expect(M.multiply(
       [
         [1, 2, 1],
         [2, 1, 2],
         [1, 2, 1]
       ], [
         [0, 1, 0],
         [1, 0, 1],
         [0, 1, 0]
       ]
     )).toEqual(
       [
         [0, 2, 0],
         [2, 0, 2],
         [0, 2, 0]
       ]
     );
   });

   it("M.multiply Depth, columns and/or rows of input do not match multiply.", function() {
     expect(() => {
       M.multiply(
         [
           [1, 2, 1],
           [2, 1, 2],
           [1, 2, 1]
         ], [
           [0, 1],
           [1, 0],
           [0, 1]
         ]
       )
     }).toThrow("Depth, columns and/or rows of input do not match value.");
   });

   it("M.multiply Should result in a 3x3 array where the input is multiplied by 2.", function() {
     expect(M.multiply(
       [
         [1, 2, 1],
         [2, 1, 2],
         [1, 2, 1]
       ],
       2
     )).toEqual(
       [
         [2, 4, 2],
         [4, 2, 4],
         [2, 4, 2]
       ]
     );
   });

   it("M.dot2D Should result in a 3x1 array.", function() {
     expect(M.dot2D(
       [
         [1, 2, 1],
         [2, 1, 2],
         [1, 2, 1]
       ], [
         [2],
         [1],
         [2]
       ]
     )).toEqual(
       [
         [6],
         [9],
         [6]
       ]
     );
   });

   it("M.dot2D The columns of the input should match the rows of multiply.", function() {
     expect(() => {
       M.dot2D(
         [
           [1, 2, 1],
           [2, 1, 2],
           [1, 2, 1]
         ], [
           [2],
           [1]
         ]
       )
     }).toThrow("The columns of the input should match the rows of multiply.");
   });

   it("M.sub2D Should result in a 2x2 array.", function() {
     expect(M.sub2D(
       [
         [2, 1, 2, 1],
         [1, 2, 1, 2],
         [2, 1, 2, 1],
         [1, 2, 1, 2],
       ], 1, 2, 1, 2
     )).toEqual(
       [
         [2, 1],
         [1, 2]
       ]
     );
   });

   it("M.sub2D The field r_start and r_length combined should be smaller or equal to 4.", function() {
     expect(() => {
       M.sub2D(
         [
           [2, 1, 2, 1],
           [1, 2, 1, 2],
           [2, 1, 2, 1],
           [1, 2, 1, 2],
         ], 3, 2, 1, 2
       )
     }).toThrow("The field r_start and r_length combined should be smaller or equal to 4.");
   });

   it("M.sub2D The field c_start and c_length combined should be smaller or equal to 4.", function() {
     expect(() => {
       M.sub2D(
         [
           [2, 1, 2, 1],
           [1, 2, 1, 2],
           [2, 1, 2, 1],
           [1, 2, 1, 2],
         ], 1, 2, 3, 2
       )
     }).toThrow("The field c_start and c_length combined should be smaller or equal to 4.");
   });

   it("M.pad2D Should result in the input to be padded with 0 on all sides.", function() {
     expect(M.pad2D(
       [
         [2, 1, 2],
         [1, 2, 1],
         [2, 1, 2]
       ], {
         t: 1,
         r: 1,
         b: 1,
         l: 1
       }
     )).toEqual(
       [
         [0, 0, 0, 0, 0],
         [0, 2, 1, 2, 0],
         [0, 1, 2, 1, 0],
         [0, 2, 1, 2, 0],
         [0, 0, 0, 0, 0]
       ]
     );
   });

   it("M.pad2D The field padding.t should be larger or equal to 0.", function() {
     expect(() => {
       M.pad2D(
         [
           [2, 1, 2],
           [1, 2, 1],
           [2, 1, 2]
         ], {
           t: -1,
           r: 1,
           b: 1,
           l: 1
         }
       )
     }).toThrow("The field padding.t should be larger or equal to 0.");
   });

   it("M.pad2D The field padding.r should be larger or equal to 0.", function() {
     expect(() => {
       M.pad2D(
         [
           [2, 1, 2],
           [1, 2, 1],
           [2, 1, 2]
         ], {
           t: 1,
           r: -1,
           b: 1,
           l: 1
         }
       )
     }).toThrow("The field padding.r should be larger or equal to 0.");
   });

   it("M.pad2D The field padding.b should be larger or equal to 0.", function() {
     expect(() => {
       M.pad2D(
         [
           [2, 1, 2],
           [1, 2, 1],
           [2, 1, 2]
         ], {
           t: 1,
           r: 1,
           b: -1,
           l: 1
         }
       )
     }).toThrow("The field padding.b should be larger or equal to 0.");
   });

   it("M.pad2D The field padding.l should be larger or equal to 0.", function() {
     expect(() => {
       M.pad2D(
         [
           [2, 1, 2],
           [1, 2, 1],
           [2, 1, 2]
         ], {
           t: 1,
           r: 1,
           b: 1,
           l: -1
         }
       )
     }).toThrow("The field padding.l should be larger or equal to 0.");
   });

   it("M.transpose Should result in the input to be transposed.", function() {
     expect(M.transpose(
       [
         [1, 2],
         [3, 4],
         [5, 6],

       ]
     )).toEqual(
       [
         [1, 3, 5],
         [2, 4, 6]
       ]
     );
   });

   it("M.flip Should result in the input to be flipped.", function() {
     expect(M.flip(
       [
         [1, 2, 3],
         [4, 5, 6],
         [7, 8, 9],

       ]
     )).toEqual(
       [
         [9, 8, 7],
         [6, 5, 4],
         [3, 2, 1]
       ]
     );
   });

   it("M.map Should iterate over all values.", function() {
     expect(M.map(
       [
         [1, 1, 1],
         [1, 1, 1],
         [1, 1, 1],

       ], (pos, v) => pos.r + pos.c - v)).toEqual(
       [
         [-1, 0, 1],
         [0, 1, 2],
         [1, 2, 3]
       ]
     );
   });

   it("M.map fn should be not-null and a function.", function() {
     expect(() => {
       M.map(
         [
           [1, 2, 3],
           [4, 5, 6],
           [7, 8, 9],

         ], null);
     }).toThrow("The field fn must not be null.");

     expect(() => {
       M.map(
         [
           [1, 2, 3],
           [4, 5, 6],
           [7, 8, 9],

         ], "");
     }).toThrow("The field fn is not a function.");
   });

   it("M.loop Should iterate over all values (3x3).", function() {
     let r = M.createArray({
       r: 3,
       c: 3
     });
     M.loop(
       [
         [1, 2, 3],
         [4, 5, 6],
         [7, 8, 9],

       ], (pos, v) => {
         r.set(pos, v);
       });

     expect(r).toEqual(
       [
         [1, 2, 3],
         [4, 5, 6],
         [7, 8, 9]
       ]
     );
   });

   it("M.loop fn should be not-null and a function 3x3.", function() {
     expect(() => {
       M.loop(
         [
           [1, 2, 3],
           [4, 5, 6],
           [7, 8, 9],

         ], null);
     }).toThrow("The field fn must not be null.");

     expect(() => {
       M.loop(
         [
           [1, 2, 3],
           [4, 5, 6],
           [7, 8, 9],

         ], "");
     }).toThrow("The field fn is not a function.");
   });

   it("M.isDim1D Should return false when the input is not a one dimensional array description", function() {
     expect(M.isDim1D({})).toEqual(false);
     expect(M.isDim1D({
       d: 1,
       r: 1
     })).toEqual(false);
     expect(M.isDim1D({
       d: 1
     })).toEqual(false);
     expect(M.isDim1D({
       r: 1,
       c: 1
     })).toEqual(false);
     expect(M.isDim1D({
       d: 1,
       r: 1
     })).toEqual(false);
     expect(M.isDim1D({
       d: 1,
       c: 1
     })).toEqual(false);
   });

   it("M.isDim1D Should return true when the input is a one dimensional array description", function() {
     expect(M.isDim1D({
       c: 1
     })).toEqual(true);
   });

   it("M.isDim2D Should return false when the input is not a two dimensional array description", function() {
     expect(M.isDim2D({})).toEqual(false);
     expect(M.isDim2D({
       d: 1,
       r: 1,
       c: 1
     })).toEqual(false);
     expect(M.isDim2D({
       d: 1,
       r: 1
     })).toEqual(false);
     expect(M.isDim2D({
       d: 1
     })).toEqual(false);
     expect(M.isDim2D({
       c: 1
     })).toEqual(false);
     expect(M.isDim2D({
       r: 1
     })).toEqual(false);
     expect(M.isDim2D({
       d: 1
     })).toEqual(false);

   });

   it("M.isDim2D Should return true when the input is a two dimensional array description", function() {
     expect(M.isDim2D({
       r: 1,
       c: 1
     })).toEqual(true);
   });

   it("M.isDim3D Should return false when the input is not a three dimensional array description", function() {
     expect(M.isDim3D({})).toEqual(false);
     expect(M.isDim3D({
       d: 1,
       r: 1
     })).toEqual(false);
     expect(M.isDim3D({
       d: 1
     })).toEqual(false);
     expect(M.isDim3D({
       r: 1
     })).toEqual(false);
     expect(M.isDim3D({
       c: 1
     })).toEqual(false);
     expect(M.isDim3D({
       d: 1,
       c: 1
     })).toEqual(false);
     expect(M.isDim3D({
       r: 1,
       c: 1
     })).toEqual(false);
   });

   it("M.isDim3D Should return true when the input is a three dimensional array description", function() {
     expect(M.isDim3D({
       d: 1,
       r: 1,
       c: 1
     })).toEqual(true);
   });

 });
