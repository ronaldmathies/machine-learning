describe("DenseLayer Suite", function() {
  it("DenseLayer.config.units Should be larger then 0.", function() {
    expect(() => {
      new DenseLayer({
        units: 0
      });
    }).toThrow("The field units should be larger then 0.");
  });

  it("DenseLayer.config.input_shape.r Should be larger then 0.", function() {
    expect(() => {
      new DenseLayer({
        input_shape: {
          r: 0,
          c: 1
        }
      });
    }).toThrow("The field input_shape.r should be larger then 0.");
  });

  it("DenseLayer.config.input_shape.c Should be larger then 0.", function() {
    expect(() => {
      new DenseLayer({
        input_shape: {
          r: 1,
          c: 0
        }
      });
    }).toThrow("The field input_shape.c should be larger then 0.");
  });

  it("DenseLayer.config.learning_rate Should be larger then 0.", function() {
    expect(() => {
      new DenseLayer({
        learning_rate: 0
      });
    }).toThrow("The field learning_rate should be larger then 0.");
  });

  it("DenseLayer.config.activation_function Should not be null.", function() {
    expect(() => {
      new DenseLayer({
        activation_function: null
      });
    }).toThrow("The field activation_function must not be null.");
  });

  it("DenseLayer.config.bias_initializer Should be a function.", function() {
    expect(() => {
      new DenseLayer({
        bias_initializer: ""
      });
    }).toThrow("The field bias_initializer is not a function.");
  });

  it("DenseLayer.config.bias_initializer Should not be null.", function() {
    expect(() => {
      new DenseLayer({
        bias_initializer: null
      });
    }).toThrow("The field bias_initializer must not be null.");
  });

  it("DenseLayer.config.kernel_initializer Should be a function.", function() {
    expect(() => {
      new DenseLayer({
        kernel_initializer: ""
      });
    }).toThrow("The field kernel_initializer is not a function.");
  });

  it("DenseLayer.config.kernel_initializer Should not be null.", function() {
    expect(() => {
      new DenseLayer({
        kernel_initializer: null
      });
    }).toThrow("The field kernel_initializer must not be null.");
  });


  it("DenseLayer.next_layer Should not be null after compile.", function() {
    expect(() => {
      new DenseLayer().compile();
    }).toThrow("The field next_layer must not be null.");
  });

  it("DenseLayer.bias Should have correct dimensions and filling.", function() {
    let denseLayer = new DenseLayer({
      units: 3,
      bias_initializer: IF.constant(3)
    });
    denseLayer.compile(new DenseLayer());

    expect(denseLayer.bias)
      .toEqual([
        [3],
        [3],
        [3]
      ]);
  });

  it("DenseLayer.weight Should have correct dimensions and filling after compile.", function() {
    let denseLayer = new DenseLayer({
      units: 3,
      input_shape: {
        r: 2,
        c: 1
      },
      kernel_initializer: IF.constant(3)
    });
    denseLayer.compile(new DenseLayer());

    expect(denseLayer.weights)
      .toEqual([
        [3, 3],
        [3, 3],
        [3, 3]
      ]);
  });

  it("DenseLayer.next_layer Should be called at the end of the feedforward.", function() {
    let layerMock = jasmine.createSpyObj("layerMock", ["feedForward"]);
    let denseLayer = new DenseLayer({
      units: 3,
      input_shape: {
        r: 2,
        c: 1
      },
      activation_function: AF.none(),
      bias_initializer: IF.constant(1),
      kernel_initializer: IF.constant(3)
    });
    denseLayer.compile(layerMock);

    denseLayer.feedForward([
      [1],[0]
    ], false);

    expect(layerMock.feedForward).toHaveBeenCalledWith([[4], [4], [4]], false);
  });
});
