describe("Activation Function Suite", function() {
  it("AF.sigmoid.func Should apply activation onto matrix.", function() {
    expect(AF.sigmoid().func(
      [
        [1, 2],
        [3, 4],
      ]
    )).toEqual([
      [0.7310585786300049, 0.8807970779778823],
      [0.9525741268224334, 0.9820137900379085],
    ]);
  });

  it("AF.sigmoid.dfunc Should apply activation onto matrix.", function() {
    expect(AF.sigmoid().dfunc(
      [
        [1, 2],
        [3, 4],
      ]
    )).toEqual([
      [0, -2],
      [-6, -12],
    ]);
  });

  it("AF.sigmoid.dfunc", function() {
    let m = [
      [1, 2],
      [3, 4],
    ];

    let msf = AF.sigmoid().func(m);
    let msfd = AF.sigmoid().dfunc(msf);

    expect(msfd).toEqual([
      [0.19661193324148185, 0.10499358540350662],
      [0.045176659730912, 0.017662706213291107]
    ]);
  });

});
