describe("PoolLayer Suite", function() {
  it("PoolLayer.config.input_shape.d Should be larger then 0.", function() {
    expect(() => {
      new PoolLayer({
        input_shape: { d: -1, r: 0, c: 1 }
      });
    }).toThrow("The field input_shape.d should be larger then 0.");
  });

  it("PoolLayer.config.input_shape.r Should be larger then 0.", function() {
    expect(() => {
      new PoolLayer({
        input_shape: { d: 1, r: 0, c: 1 }
      });
    }).toThrow("The field input_shape.r should be larger then 0.");
  });

  it("PoolLayer.config.input_shape.c Should be larger then 0.", function() {
    expect(() => {
      new PoolLayer({
        input_shape: { d: 1, r: 1, c: 0 }
      });
    }).toThrow("The field input_shape.c should be larger then 0.");
  });

  it("PoolLayer.config.kernel.r Should be larger then 0.", function() {
    expect(() => {
      new PoolLayer({
        input_shape: { r: 16, c: 16 },
        kernel: { r: 0, c: 1 }
      });
    }).toThrow("The field kernel.r should be larger then 0.");
  });

  it("PoolLayer.config.kernel.c Should be larger then 0.", function() {
    expect(() => {
      new PoolLayer({
        input_shape: { r: 16, c: 16 },
        kernel: { r: 1, c: 0 }
      });
    }).toThrow("The field kernel.c should be larger then 0.");
  });

  it("PoolLayer.config.stride.r Should be larger then 0.", function() {
    expect(() => {
      new PoolLayer({
        input_shape: { r: 16, c: 16 },
        stride: { r: 0, c: 1 }
      });
    }).toThrow("The field stride.r should be larger then 0.");
  });

  it("PoolLayer.config.stride.c Should be larger then 0.", function() {
    expect(() => {
      new PoolLayer({
        input_shape: { r: 16, c: 16 },
        stride: { r: 1, c: 0 }
      });
    }).toThrow("The field stride.c should be larger then 0.");
  });

  it("PoolLayer.next_layer Should not be null after compile.", function() {
    expect(() => {
      new PoolLayer({
        input_shape: { r: 16, c: 16 }
      }).compile();
    }).toThrow("The field next_layer must not be null.");
  });

  it("PoolLayer.padding_shape is 0,0,0,0 (I=1x4x4, K=2x2, S=1x1, P=VALID)", function() {
    expect(
      new PoolLayer({
        input_shape: { d: 1, r: 4, c: 4 },
        kernel: { r: 2, c: 2 },
        stride: { r: 1, c: 1 },
        padding: "valid"
      }).padding_shape
    ).toEqual({ t: 0, r: 0, b: 0, l: 0 });
  });

  it("PoolLayer.padding_shape is 2,2,2,2 (I=1x16x16, K=5x5, S=1x1, P=SAME)", function() {
    expect(
      new PoolLayer({
        input_shape: { d: 1, r: 16, c: 16 },
        kernel: { r: 5, c: 5 },
        stride: { r: 1, c: 1 },
        padding: "same"
      }).padding_shape
    ).toEqual({ t: 2, r: 2, b: 2, l: 2 });
  });

  it("PoolLayer.padding_shape is 1,2,2,1 (I=0x16x16, K=5x5, S=2x2, P=SAME)", function() {
    expect(
      new PoolLayer({
        input_shape: { r: 16, c: 16 },
        kernel: { r: 5, c: 5 },
        stride: { r: 2, c: 2 },
        padding: "same"
      }).padding_shape
    ).toEqual({ t: 1, r: 2, b: 2, l: 1 });
  });

  it("PoolLayer.padding_shape is 0x4x4 (I=1x16x16, K=5x5, S=3x3, P=SAME)", function() {
    expect(
      new PoolLayer({
        input_shape: { d: 1, r: 16, c: 16 },
        kernel: { r: 5, c: 5 },
        stride: { r: 3, c: 3 },
        padding: "same"
      }).padding_shape
    ).toEqual({ t: 2, r: 2, b: 2, l: 2 });
  });

  it("PoolLayer.output_shape is 1x8x8 (I=1x8x8, K=3x3, S=1x1, P=SAME)", function() {
    expect(
      new PoolLayer({
        input_shape: { d: 1, r: 8, c: 8 },
        kernel: { r: 3, c: 3 },
        stride: { r: 1, c: 1 },
        padding: "same",
        pool_func: maxPool
      }).output_shape
    ).toEqual({ d: 1, r: 8, c: 8 });
  });

  it("PoolLayer.output_shape is 0x6x6 (I=1x8x8, K=3x3, S=1x1, P=SAME)", function() {
    expect(
      new PoolLayer({
        input_shape: { r: 8, c: 8 },
        kernel: { r: 3, c: 3 },
        stride: { r: 1, c: 1 },
        padding: "valid",
        pool_func: maxPool
      }).output_shape
    ).toEqual({ r: 6, c: 6 });
  });

  it("PoolLayer.values is 1x8x8 (I=1x8x8, K=3x3, S=1x1, P=SAME)", function() {
    expect(
      new PoolLayer({
        input_shape: { d: 1, r: 8, c: 8 },
        kernel: { r: 3, c: 3 },
        stride: { r: 1, c: 1 },
        padding: "same",
        pool_func: maxPool
      }).values
    ).toEqual(
      [
        [
          new Array(8),
          new Array(8),
          new Array(8),
          new Array(8),
          new Array(8),
          new Array(8),
          new Array(8),
          new Array(8)
        ]
      ]
    );
  });

  it("PoolLayer.values is 6x6 (I=1x8x8, K=3x3, S=1x1, P=same)", function() {
    expect(
      new PoolLayer({
        input_shape: { r: 8, c: 8 },
        kernel: { r: 3, c: 3 },
        stride: { r: 1, c: 1 },
        padding: "valid",
        pool_func: maxPool
      }).values
    ).toEqual(
      [
        new Array(6),
        new Array(6),
        new Array(6),
        new Array(6),
        new Array(6),
        new Array(6),
      ]
    );
  });

  it("PoolLayer.full integration (I=0x4x4, K=2x2, S=1x1, P=valid).", function() {
    let layerMock = jasmine.createSpyObj("layerMock", {
      "feedForward": null,
      "train": [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
      ]
    });

    let poolLayer = new PoolLayer({
      input_shape: { r: 4, c: 4 },
      kernel: { r: 2, c: 2 },
      pool_func: maxPool,
      stride: { c: 1, r: 1 },
      padding: "valid"
    });
    poolLayer.compile(layerMock);

    poolLayer.feedForward([
      [1, 2, 4, 3],
      [0, 3, 2, 7],
      [6, 5, 3, 4],
      [1, 5, 7, 2]
    ], false);

    expect(layerMock.feedForward).toHaveBeenCalledWith([
      [3, 4, 7],
      [6, 5, 7],
      [6, 7, 7]
    ], false);

    expect(poolLayer.pool_masks).toEqual(
      [
        [{ r: 1, c: 1 }, { r: 0, c: 2 }, { r: 1, c: 3 }],
        [{ r: 2, c: 0 }, { r: 2, c: 1 }, { r: 1, c: 3 }],
        [{ r: 2, c: 0 }, { r: 3, c: 2 }, { r: 3, c: 2 }]
      ]
    );

    poolLayer.train([], []);

    expect(poolLayer.errors).toEqual([
      [00, 00, 02, 00],
      [00, 01, 00, 09],
      [11, 05, 00, 00],
      [00, 00, 17, 00]
    ]);
  });

  it("PoolLayer.full integration (I=1x4x4, K=2x2, S=1x1, P=valid).", function() {
    let layerMock = jasmine.createSpyObj("layerMock", {
      "feedForward": null,
      "train": [
        [
          [1, 2, 3],
          [4, 5, 6],
          [7, 8, 9]
        ]
      ]
    });

    let poolLayer = new PoolLayer({
      input_shape: { d: 1, r: 4, c: 4 },
      kernel: { r: 2, c: 2 },
      pool_func: maxPool,
      stride: { c: 1, r: 1 },
      padding: "valid"
    });
    poolLayer.compile(layerMock);

    poolLayer.feedForward(
      [
        [
          [1, 2, 4, 3],
          [0, 3, 2, 7],
          [6, 5, 3, 4],
          [1, 5, 7, 2]
        ]
      ],
      false);

    expect(layerMock.feedForward).toHaveBeenCalledWith(
      [
        [
          [3, 4, 7],
          [6, 5, 7],
          [6, 7, 7]
        ]
      ], false);

    expect(poolLayer.pool_masks).toEqual(
      [
        [
          [{ r: 1, c: 1 }, { r: 0, c: 2 }, { r: 1, c: 3 }],
          [{ r: 2, c: 0 }, { r: 2, c: 1 }, { r: 1, c: 3 }],
          [{ r: 2, c: 0 }, { r: 3, c: 2 }, { r: 3, c: 2 }]
        ]
      ]
    );

    poolLayer.train([], []);

    expect(poolLayer.errors).toEqual(
      [
        [
          [00, 00, 02, 00],
          [00, 01, 00, 09],
          [11, 05, 00, 00],
          [00, 00, 17, 00]
        ]
      ]
    );
  });

  it("PoolLayer.full integration (I=0x4x4, K=2x2, S=1x1, P=same).", function() {
    let layerMock = jasmine.createSpyObj("layerMock", {
      "feedForward": null,
      "train": [
        [01, 02, 03, 04],
        [05, 06, 07, 08],
        [09, 10, 11, 12],
        [13, 14, 15, 16],
      ]
    });

    let poolLayer = new PoolLayer({
      input_shape: { r: 4, c: 4 },
      kernel: { r: 2, c: 2 },
      pool_func: maxPool,
      stride: { c: 1, r: 1 },
      padding: "same"
    });
    poolLayer.compile(layerMock);

    poolLayer.feedForward(
      [
        [1, 2, 4, 3],
        [0, 3, 2, 7],
        [6, 5, 3, 4],
        [1, 5, 7, 2]
      ],
      false);

    expect(layerMock.feedForward).toHaveBeenCalledWith(
      [
        [3, 4, 7, 7],
        [6, 5, 7, 7],
        [6, 7, 7, 4],
        [5, 7, 7, 2]
      ],
      false);

    expect(poolLayer.pool_masks).toEqual(
      [
        [{ r: 1, c: 1 }, { r: 0, c: 2 }, { r: 1, c: 3 }, { r: 1, c: 3 }],
        [{ r: 2, c: 0 }, { r: 2, c: 1 }, { r: 1, c: 3 }, { r: 1, c: 3 }],
        [{ r: 2, c: 0 }, { r: 3, c: 2 }, { r: 3, c: 2 }, { r: 2, c: 3 }],
        [{ r: 3, c: 1 }, { r: 3, c: 2 }, { r: 3, c: 2 }, { r: 3, c: 3 }]
      ]
    );

    poolLayer.train([], []);

    expect(poolLayer.errors).toEqual(
      [
        [00, 00, 02, 00],
        [00, 01, 00, 22],
        [14, 06, 00, 12],
        [00, 13, 50, 16]
      ]
    );
  });
});
