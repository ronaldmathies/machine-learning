describe("ConvolutionalLayer Suite", function() {
  it("ConvolutionalLayer.config.input_shape.d Should be larger then 0.", function() {
    expect(() => {
      new ConvolutionalLayer({
        input_shape: { d: 0, r: 1, c: 1 },
        kernel: { d: 1, r: 1, c: 1 }
      });
    }).toThrow("The field input_shape.d should be larger then 0.");
  });

  it("ConvolutionalLayer.config.input_shape.r Should be larger then 0.", function() {
    expect(() => {
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 0, c: 1 },
        kernel: { d: 1, r: 1, c: 1 }
      });
    }).toThrow("The field input_shape.r should be larger then 0.");
  });

  it("ConvolutionalLayer.config.input_shape.c Should be larger then 0.", function() {
    expect(() => {
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 1, c: 0 },
        kernel: { d: 1, r: 1, c: 1 }
      });
    }).toThrow("The field input_shape.c should be larger then 0.");
  });

  it("ConvolutionalLayer.config.kernel.d Should be larger then 0.", function() {
    expect(() => {
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 1, c: 1 },
        kernel: { d: 0, r: 1, c: 1 }
      });
    }).toThrow("The field kernel.d should be larger then 0.");
  });

  it("ConvolutionalLayer.config.kernel.r Should be larger then 0.", function() {
    expect(() => {
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 1, c: 1 },
        kernel: { d: 1, r: 0, c: 1 }
      });
    }).toThrow("The field kernel.r should be larger then 0.");
  });

  it("ConvolutionalLayer.config.kernel.c Should be larger then 0.", function() {
    expect(() => {
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 1, c: 1 },
        kernel: { d: 1, r: 1, c: 0 }
      });
    }).toThrow("The field kernel.c should be larger then 0.");
  });

  it("ConvolutionalLayer.config.stride.r Should be larger then 0.", function() {
    expect(() => {
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 1, c: 1 },
        kernel: { d: 1, r: 1, c: 1 },
        stride: { r: 0, c: 1 }
      });
    }).toThrow("The field stride.r should be larger then 0.");
  });

  it("ConvolutionalLayer.config.stride.c Should be larger then 0.", function() {
    expect(() => {
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 1, c: 1 },
        kernel: { d: 1, r: 1, c: 1 },
        stride: { r: 1, c: 0 }
      });
    }).toThrow("The field stride.c should be larger then 0.");
  });

  it("ConvolutionalLayer.config.kernel_initializer Should be a function.", function() {
    expect(() => {
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 1, c: 1 },
        kernel: { d: 1, r: 1, c: 1 },
        kernel_initializer: ""
      });
    }).toThrow("The field kernel_initializer is not a function.");
  });

  it("ConvolutionalLayer.config.kernel_initializer Should not be null.", function() {
    expect(() => {
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 1, c: 1 },
        kernel: { d: 1, r: 1, c: 1 },
        kernel_initializer: null
      });
    }).toThrow("The field kernel_initializer must not be null.");
  });

  it("ConvolutionalLayer.next_layer Should not be null after compile.", function() {
    expect(() => {
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 16, c: 16 },
        kernel: { d: 2, r: 2, c: 2 }
      }).compile();
    }).toThrow("The field next_layer must not be null.");
  });

  it("ConvolutionalLayer.weights Should have correct dimensions and filling after compile.", function() {
    let convolutionalLayer = new ConvolutionalLayer({
      input_shape: { d: 1, r: 16, c: 16 },
      kernel: { d: 2, r: 2, c: 2 },
      kernel_initializer: IF.constant(3)
    });

    expect(convolutionalLayer.weights)
      .toEqual(
        [
          [
            [3, 3],
            [3, 3]
          ],
          [
            [3, 3],
            [3, 3]
          ]
        ]);
  });

  it("ConvolutionalLayer.padding_shape is 0,0,0,0 (I=1x4x4, K=2x2x2, S=1x1, P=VALID)", function() {
    expect(
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 4, c: 4 },
        kernel: { d: 2, r: 2, c: 2 },
        stride: { r: 1, c: 1 },
        padding: "valid"
      }).padding_shape
    ).toEqual({ t: 0, r: 0, b: 0, l: 0 });
  });

  it("ConvolutionalLayer.padding_shape is 2,2,2,2 (I=1x16x16, K=1x5x5, S=1x1, P=SAME)", function() {
    expect(
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 16, c: 16 },
        kernel: { d: 1, r: 5, c: 5 },
        stride: { r: 1, c: 1 },
        padding: "same"
      }).padding_shape
    ).toEqual({ t: 2, r: 2, b: 2, l: 2 });
  });

  it("ConvolutionalLayer.padding_shape is 1,2,2,1 (I=1x16x16, K=2x5x5, S=2x2, P=SAME)", function() {
    expect(
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 16, c: 16 },
        kernel: { d: 2, r: 5, c: 5 },
        stride: { r: 2, c: 2 },
        padding: "same"
      }).padding_shape
    ).toEqual({ t: 1, r: 2, b: 2, l: 1 });
  });

  it("ConvolutionalLayer.padding_shape is 0x4x4 (I=1x16x16, K=2x5x5, S=3x3, P=SAME)", function() {
    expect(
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 16, c: 16 },
        kernel: { d: 2, r: 5, c: 5 },
        stride: { r: 3, c: 3 },
        padding: "same"
      }).padding_shape
    ).toEqual({ t: 2, r: 2, b: 2, l: 2 });
  });

  it("ConvolutionalLayer.output_shape is 1x8x8 (I=1x8x8, K=2x3x3, S=1x1, P=SAME)", function() {
    expect(
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 8, c: 8 },
        kernel: { d: 2, r: 3, c: 3 },
        stride: { r: 1, c: 1 },
        padding: "same"
      }).output_shape
    ).toEqual({ d: 2, r: 8, c: 8 });
  });

  it("ConvolutionalLayer.output_shape is 0x6x6 (I=1x8x8, K=2x3x3, S=1x1, P=SAME)", function() {
    expect(
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 8, c: 8 },
        kernel: { d: 2, r: 3, c: 3 },
        stride: { r: 1, c: 1 },
        padding: "valid"
      }).output_shape
    ).toEqual({ d: 2, r: 6, c: 6 });
  });

  it("ConvolutionalLayer.values is 2x4x4 (I=2x4x4, K=3x3, S=1x1, P=SAME)", function() {
    expect(
      new ConvolutionalLayer({
        input_shape: { d: 2, r: 4, c: 4 },
        kernel: { d: 2, r: 3, c: 3 },
        stride: { r: 1, c: 1 },
        padding: "same"
      }).values
    ).toEqual(M.createArray({d: 4, r: 4, c: 4}, true));
  });

  it("ConvolutionalLayer.values is 2x6x6 (I=1x8x8, K=2x3x3, S=1x1, P=same)", function() {
    expect(
      new ConvolutionalLayer({
        input_shape: { d: 1, r: 8, c: 8 },
        kernel: { d: 2, r: 3, c: 3 },
        stride: { r: 1, c: 1 },
        padding: "valid"
      }).values
    ).toEqual(M.createArray({d: 2, r: 6, c: 6}, true));
  });

  it("ConvolutionalLayer.full integration (I=0x8x8, K=2x2, S=1x1, P=valid).", function() {
    let layerMock = jasmine.createSpyObj("layerMock", {
      "feedForward": null,
      "train": [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0]
      ]
    });

    let convolutionalLayer = new ConvolutionalLayer({
      input_shape: { d: 1, r: 5, c: 5 },
      kernel: { d: 1, r: 3, c: 3 },
      pool_func: maxPool,
      stride: { c: 1, r: 1 },
      padding: "valid",
      kernel_initializer: IF.zeros()
    });
    convolutionalLayer.compile(layerMock);

    convolutionalLayer.weights[0] = [
      [1, 0, 1],
      [0, 1, 0],
      [1, 0, 1]
    ];

    convolutionalLayer.feedForward([
      [
        [1, 1, 1, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 1, 1, 1],
        [0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0]
      ]
    ], false);

    expect(layerMock.feedForward).toHaveBeenCalledWith([
      [
        [4, 3, 4],
        [2, 4, 3],
        [2, 3, 4]
      ]
    ], false);

    // expect(poolLayer.pool_masks).toEqual(
    //   [
    //     [{ r: 1, c: 1 }, { r: 0, c: 2 }, { r: 1, c: 3 }],
    //     [{ r: 2, c: 0 }, { r: 2, c: 1 }, { r: 1, c: 3 }],
    //     [{ r: 2, c: 0 }, { r: 3, c: 2 }, { r: 3, c: 2 }]
    //   ]
    // );

    // poolLayer.train([], []);

    // expect(poolLayer.errors).toEqual([
    //   [00, 00, 02, 00],
    //   [00, 01, 00, 09],
    //   [11, 05, 00, 00],
    //   [00, 00, 17, 00]
    // ]);
  });

  it("ConvolutionalLayer.full integration (I=0x8x8, K=2x2, S=1x1, P=same).", function() {
    let layerMock = jasmine.createSpyObj("layerMock", {
      "feedForward": null,
      "train": [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0]
      ]
    });

    let convolutionalLayer = new ConvolutionalLayer({
      input_shape: { d: 1, r: 5, c: 5 },
      kernel: { d: 1, r: 3, c: 3 },
      pool_func: maxPool,
      stride: { c: 1, r: 1 },
      padding: "same",
      kernel_initializer: IF.zeros()
    });
    convolutionalLayer.compile(layerMock);

    convolutionalLayer.weights[0] = [
      [1, 0, 1],
      [0, 1, 0],
      [1, 0, 1]
    ];

    convolutionalLayer.feedForward([
      [
        [1, 1, 1, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 1, 1, 1],
        [0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0]
      ]
    ], false);

    expect(layerMock.feedForward).toHaveBeenCalledWith([
      [
        [2, 2, 3, 1, 1],
        [1, 4, 3, 4, 1],
        [1, 2, 4, 3, 3],
        [1, 2, 3, 4, 1],
        [0, 2, 2, 1, 1]
      ]
    ], false);

    // expect(poolLayer.pool_masks).toEqual(
    //   [
    //     [{ r: 1, c: 1 }, { r: 0, c: 2 }, { r: 1, c: 3 }],
    //     [{ r: 2, c: 0 }, { r: 2, c: 1 }, { r: 1, c: 3 }],
    //     [{ r: 2, c: 0 }, { r: 3, c: 2 }, { r: 3, c: 2 }]
    //   ]
    // );

    // poolLayer.train([], []);

    // expect(poolLayer.errors).toEqual([
    //   [00, 00, 02, 00],
    //   [00, 01, 00, 09],
    //   [11, 05, 00, 00],
    //   [00, 00, 17, 00]
    // ]);
  });

});
