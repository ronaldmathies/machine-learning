describe("DropoutLayer Suite", function() {
  it("DropoutLayer.config.input_shape.d Should be larger -1.", function() {
    expect(() => {
      new DropoutLayer({
        input_shape: { d: -1, r: 1, c: 1 }
      });
    }).toThrow("The field input_shape.d should be larger then -1.");
  });

  it("DropoutLayer.config.input_shape.r Should be larger then -1.", function() {
    expect(() => {
      new DropoutLayer({
        input_shape: { d: 1, r: -1, c: 1 }
      });
    }).toThrow("The field input_shape.r should be larger then -1.");
  });

  it("DropoutLayer.config.input_shape.c Should be larger then 0.", function() {
    expect(() => {
      new DropoutLayer({
        input_shape: { d: 1, r: 1, c: 0 }
      });
    }).toThrow("The field input_shape.c should be larger then 0.");
  });

  it("DropoutLayer.config.rate Should be larger then 0.", function() {
    expect(() => {
      new DropoutLayer({
        input_shape: { d: 0, r: 4, c: 4 },
        rate: 0
      });
    }).toThrow("The field rate should be larger then 0.");
  });

  it("DropoutLayer.config.rate Should be smaller then 1.", function() {
    expect(() => {
      new DropoutLayer({
        input_shape: { d: 0, r: 4, c: 4 },
        rate: 1
      });
    }).toThrow("The field rate should be smaller then 1.");
  });

  it("DropoutLayer.config.seed Should be larger then 0.", function() {
    expect(() => {
      new DropoutLayer({
        input_shape: { d: 0, r: 4, c: 4 },
        seed: 0
      });
    }).toThrow("The field seed should be larger then 0.");
  });

  it("DropoutLayer.next_layer Should not be null after compile.", function() {
    expect(() => {
      new DropoutLayer({
        input_shape: { d: 0, r: 4, c: 4 }
      }).compile();
    }).toThrow("The field next_layer must not be null.");
  });

  /*it("DropoutLayer.full integration (I=0x0x256).", function() {
    let layerMock = jasmine.createSpyObj("layerMock", {
      "feedForward": null,
      "train": [
        [2],
        [3]
      ]
    });
    let reshapeLayer = new ReshapeLayer({
      input_shape: { d: 0, r: 0, c: 256 },
      target_shape: { d: 0, r: 2, c: 1 }
    });
    reshapeLayer.compile(layerMock);

    reshapeLayer.feedForward(
      [
        [1, 2]
      ], false);

    expect(layerMock.feedForward).toHaveBeenCalledWith([
      [1],
      [2]
    ], false);

    reshapeLayer.train([]);

    expect(reshapeLayer.errors).toEqual(
      [
        [2, 3]
      ]
    );
  });*/

});
