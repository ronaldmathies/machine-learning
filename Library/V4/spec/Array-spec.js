describe("Array Suite", function() {

  it("is1d Should return false when the input is not a one dimensional array", function() {
    expect([
      [1]
    ].is1d()).toEqual(false);
  });

  it("is1d Should return true when the input is a one dimensional array", function() {
    expect([1].is1d()).toEqual(true);
  });

  it("is2d Should return false when the input is not a two dimensional array", function() {
    expect([1].is2d()).toEqual(false);
    expect([
      [
        [1]
      ]
    ].is2d()).toEqual(false);
  });

  it("is2d Should return true when the input is a two dimensional array", function() {
    expect([
      [1]
    ].is2d()).toEqual(true);
  });

  it("is3d Should return false when the input is not a three dimensional array", function() {
    expect([1].is3d()).toEqual(false);
    expect([
      [1]
    ].is3d()).toEqual(false);
  });

  it("is3d Should return true when the input is a three dimensional array", function() {
    expect([
      [
        [1]
      ]
    ].is3d()).toEqual(true);
  });

  it("max Should result in highest number to be 3 in the 0x3 matrix.", function() {
    expect(
      [1, 3, 2].max()
    ).toEqual({
      v: 3,
      c: 1
    });
  });

  it("max Should result in highest number to be 8 in a 3x3 matrix.", function() {
    expect(
      [
        [1, 1, 1],
        [1, 8, 1],
        [1, 1, 1]
      ].max()
    ).toEqual({
      v: 8,
      r: 1,
      c: 1
    });
  });

  it("max Should result in highest number to be 8 in a 2x3x3 matrix.", function() {
    expect(
      [
        [
          [1, 1, 1],
          [1, 5, 1]
        ],
        [
          [1, 1, 1],
          [1, 8, 1]
        ]
      ].max()
    ).toEqual({
      v: 8,
      d: 1,
      r: 1,
      c: 1
    });
  });

});
