"use strict";

let oDefaultConfig = {
  defaults: {
    units: 1.0,
    input_shape: {r: 1.0},
    activation_function: AF.sigmoid(),
    learning_rate: 0.1,
    bias_initializer: IF.random(),
    kernel_initializer: IF.random()
  },

  get: function(target, name) {
    return target.hasOwnProperty(name) ? target[name] : oDefaultConfig.defaults[name];
  }
};

class OutputLayer extends Layer {

  constructor(config) {
    super("OL");

    this.p = new Proxy(config == null ? {} : config, oDefaultConfig);

    A.isLargerThen("units", this.p.units, 0);
    A.isLargerThen("input_shape.r", this.p.input_shape.r, 0);

    A.isNotNull("activation_function", this.p.activation_function);
    A.isLargerThen("learning_rate", this.p.learning_rate, 0);
    A.isFunction("bias_initializer", this.p.bias_initializer);
    A.isFunction("kernel_initializer", this.p.kernel_initializer);

    this.values = null;
    this.values_activated = null;
    this.errors = null;
  }

  compile() {
    super.compile();

    this.bias = M.map(M.createArray({d: 0, r: this.p.units, c: 1}), this.p.bias_initializer);
    this.weights = M.map(M.createArray({d: 0, r: this.p.units, c: this.p.input_shape.r}), this.p.kernel_initializer);
  }

  feedForward(input, training = false) {
    super.feedForward();

    this.values = M.add(M.dot2D(this.weights, input), this.bias);
    this.values_activated = this.p.activation_function.func(this.values);
  }

  train(input, targets) {
    // First calculate the output error based on the (activated) values random
    // the target we were suppose to reach.
    let values = this.p.activation_function.useOriginalValues ? this.values : this.values_activated;
    let errors = M.subtract(targets, values, true);

    // Calculate the gradient so we now how much we need to adjust our weights.
    // dfunc(values) * error * learning_rate
    let gradients = M.multiply(M.multiply(this.p.activation_function.dfunc(values), errors), this.p.learning_rate);

    // Calculate the hidden to output deltas
    // gradients * hidden_valuesT
    let weights_deltas = M.dot2D(gradients, M.transpose(input));

    // Adjust the hidden -> output weights with the deltas
    M.add(this.weights, weights_deltas);

    // Adjust the output bias.
    M.add(this.bias, gradients);

    return M.dot2D(M.transpose(this.weights), errors);
  }

}
