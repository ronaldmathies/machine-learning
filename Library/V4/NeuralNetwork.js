"use strict";
//https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/
class NeuralNetwork {

  constructor(layers) {
    this.layers = layers;
  }

  compile() {
    this.layers.forEach((layer, index) => {
      let is_first_layer = index == 0;
      let is_last_layer = index == this.layers.length;

      if (!is_last_layer) {
        layer.compile(this.layers[index + 1]);
      }

    });
  }

  predict(inputs) {
    this.layers[0].feedForward(inputs);

    return this.layers.slice(-1)[0].values_activated;
  }

  train(inputs, targets) {
    this.layers[0].feedForward(inputs);
    this.layers[0].train(inputs, targets);
  }

}
