"use strict";

class M {

  static createArray(dim, fill = false) {
    if (M.isDim1D(dim)) {
      if (fill) {
        return new Array(dim.c).fill()
          .map(() => 0.0);
      }
      return new Array(dim.c);
    } else if (M.isDim2D(dim)) {
      return new Array(dim.r).fill()
        .map(() => fill ? new Array(dim.c).fill(0.0) : new Array(dim.c));
    } else if (M.isDim3D(dim)) {
      return new Array(dim.d).fill()
        .map(() => new Array(dim.r).fill()
          .map(() => fill ? new Array(dim.c).fill(0.0) : new Array(dim.c)));
    }

    throw ('No valid dimensions supplied.');
  }

  static reshape(input, output_shape) {
    let input_shape = input.dim();

    if (!NNConfig.disable_validations) {
      let total_input_digits = input_shape.r * input_shape.c * (typeof input_shape.d !== 'undefined' ? input_shape.d : 1);
      let total_output_digits = output_shape.r * output_shape.c * (typeof output_shape.d !== 'undefined' ? output_shape.d : 1);
      if (total_input_digits !== total_output_digits) {
        throw ('The total number of input digits will not fit the total number of output digits.');
      }
    }

    let _input = M.createArray({ c: input_shape.r * input_shape.c * (input.is2d() ? 1 : input_shape.d) });

    let p = 0;
    M.loop(input, (_, v) => _input[p++] = v);

    let output = M.createArray(output_shape);
    let depth_size = output_shape.c * output_shape.r;

    let c = 0;
    M.loop(_input, (_, v) => {
      if (M.isDim2D(output_shape)) {
        let _i = (c - (c % output_shape.c)) / output_shape.c;
        let _j = c % output_shape.c;
        output[_i][_j] = v;
      } else {
        let _d = (c - (c % depth_size)) / depth_size;
        let _r = c - (_d * depth_size);
        let _i = (_r - (_r % output_shape.c)) / output_shape.c;
        let _j = _r % output_shape.c;

        output[_d][_i][_j] = v;
      }

      c++;
    });

    return output;
  }

  static avg2D(input) {
    let dim = input.dim();

    let avg = 0.0;
    M.loop(input, (_, v) => avg += v);
    return avg / (dim.r * dim.c);
  }

  /* private */
  static basicArithmatic(input, value, clone = false, fn) {
    let result = clone ? M.clone(input) : input;

    if (value instanceof Array) {
      if (!NNConfig.disable_validations) {
        A.isDimEqual("input", input.dim(), "value", value.dim());
      }

      return M.loop(result, (pos) => {
        result.set(pos, fn(input.get(pos), value.get(pos)));
      });
    } else {
      A.isNumber("value", value);
      return M.map(result, (_, v) => fn(v, value));
    }
  }

  static add(input, add, clone = false) {
    return this.basicArithmatic(input, add, clone, (i, v) => i + v);
  }

  static subtract(input, subtract, clone = false) {
    return this.basicArithmatic(input, subtract, clone, (i, v) => i - v);
  }

  static multiply(input, multiply, clone = false) {
    return this.basicArithmatic(input, multiply, clone, (i, v) => i * v);
  }

  static dot2D(input, multiply) {
    let dim_i = input.dim();
    let dim_m = multiply.dim();

    if (dim_i.c !== dim_m.r) {
      throw ('The columns of the input should match the rows of multiply.')
    }

    let result = M.createArray({ r: dim_i.r, c: dim_m.c });

    let r = M.map(result,
      (pos) => {
        let sum = 0.0;
        for (let k = 0; k < dim_i.c; k++) {
          sum += input[pos.r][k] * multiply[k][pos.c];
        }
        return sum;
      });
    return r;
  }

  static sub2D(input, r_start, r_length, c_start, c_length) {
    let dim = input.dim();

    if (!NNConfig.disable_validations) {
      A.isSmallerThenOrEqualTo("r_start and r_length combined", r_start + r_length, dim.r);
      A.isSmallerThenOrEqualTo("c_start and c_length combined", c_start + c_length, dim.c);
    }

    let c_length_b = Math.min(dim.c - c_start, c_length);
    let r_length_b = Math.min(dim.r - r_start, r_length);

    let result = M.createArray({ r: r_length_b, c: c_length_b });

    for (let i = r_start, r = r_start + r_length_b; i < r; i++) {
      for (let j = c_start, c = c_start + c_length_b; j < c; j++) {
        result[i - r_start][j - c_start] = input[i][j];
      }
    }

    return result;
  }

  static pad2D(input, padding) {
    let dim_i = input.dim();

    if (!NNConfig.disable_validations) {
      A.isLargerThenOrEqualTo("padding.t", padding.t, 0);
      A.isLargerThenOrEqualTo("padding.r", padding.r, 0);
      A.isLargerThenOrEqualTo("padding.b", padding.b, 0);
      A.isLargerThenOrEqualTo("padding.l", padding.l, 0);
    }

    let input_size_with_padding = {
      r: dim_i.r + padding.t + padding.b,
      c: dim_i.c + padding.l + padding.r
    }

    let r = M.createArray(input_size_with_padding, true);

    M.loop(input, (pos, v) => {
      r[padding.t + pos.r][padding.l + pos.c] = v;
    });

    return r;
  }

  static transpose(input, copy = false) {
    let dim = input.dim();

    if (!NNConfig.disable_validations) {
      if (input.is1d() || input.is3d()) {
        throw ('Transpose can only be perfomed on a 2D matrix.');
      }
    }

    let r = M.createArray({ r: dim.c, c: dim.r });
    return M.map(r, (pos) => input[pos.c][pos.r]);
  }

  static flip(input) {
    let dim = input.dim();

    if (!NNConfig.disable_validations) {
      if (input.is1d() || input.is3d()) {
        throw ('Flip can only be perfomed on a 2D matrix.');
      }
    }

    let r = M.createArray(dim);
    return M.map(r, (pos) => input[dim.r - 1 - pos.r][dim.c - 1 - pos.c]);
  }

  static map(input, fn) {
    A.isFunction("fn", fn);
    return M.loop(input, (pos) => input.set(pos, fn(pos, input.get(pos))));
  }

  static loop(input, fn) {
    A.isFunction("fn", fn);
    let dim = input.dim();

    if (input.is1d()) {
      for (let c = 0, _c = dim.c; c < _c; c++) {
        fn({ c: c }, input[c]);
      }
    } else if (input.is2d()) {
      for (let r = 0, _r = dim.r; r < _r; r++) {
        for (let c = 0, _c = dim.c; c < _c; c++) {
          fn({r: r, c: c}, input[r][c]);
        }
      }
    } else if (input.is3d()) {
      for (let d = 0, _d = dim.d; d < _d; d++) {
        for (let r = 0, _r = dim.r; r < _r; r++) {
          for (let c = 0, _c = dim.c; c < _c; c++) {
            fn({d: d, r: r, c: c}, input[d][r][c]);
          }
        }
      }
    }

    return input;
  }

  static isDim1D(dim) {
    return typeof dim.d === "undefined" && typeof dim.r === "undefined" && typeof dim.c === "number";
  }

  static isDim2D(dim) {
    return typeof dim.d === "undefined" && typeof dim.r === "number" && typeof dim.c === "number";
  }

  static isDim3D(dim) {
    return typeof dim.d === "number" && typeof dim.r === "number" && typeof dim.c === "number";
  }

  static dimAdd(dim1, dim2) {
    return { d: dim1.d + dim2.d, r: dim1.r + dim2.r, c: dim1.c + dim2.c };
  }

  static clone(input) {
    let dim = input.dim();
    let clone = M.createArray(dim);
    M.loop(input, (pos, v) => clone.set(pos, v));
    return clone;
  }

}
