class IF {

  static zeros() {
    return function() {
      return 0.0;
    };
  }

  static ones() {
    return function() {
      return 1.0;
    };
  }

  static constant(c) {
    A.isNumber("c", c);

    return function() {
      return c;
    }
  }

  static random() {
    return function() {
      return Math.random() * 2.0 - 1.0;
    }
  }

  static random(seed) {
    let generator = new Math.seedrandom(seed);
    return function() {
      return generator() * 2.0 - 1.0;
    }
  }

}
