"use strict";

let dropoutDefaultConfig = {
  defaults: {
    input_shape: { d: 0, r: 0, c: 0 },
    rate: 0.5,
    seed: 1337
  },

  get: function(target, name) {
    return target.hasOwnProperty(name) ? target[name] : dropoutDefaultConfig.defaults[name];
  }
};

class DropoutLayer extends Layer {

  constructor(config) {
    super("RL");

    this.p = new Proxy(config == null ? {} : config, dropoutDefaultConfig);

    A.isLargerThen("input_shape.d", this.p.input_shape.d, -1);
    A.isLargerThen("input_shape.r", this.p.input_shape.r, -1);
    A.isLargerThen("input_shape.c", this.p.input_shape.c, 0);
    A.isLargerThen("rate", this.p.rate, 0);
    A.isSmallerThen("rate", this.p.rate, 1);
    A.isLargerThen("seed", this.p.seed, 0);

    this.generator = new Math.seedrandom(this.p.seed);
    this.values = [];
  }

  compile(next_layer) {
    super.compile();

    this.next_layer = A.isNotNull("next_layer", next_layer);
  }

  feedForward(inputs, training = false) {
    super.feedForward();

    if (training) {
      this.mask = M.createArray(this.p.input_shape);
      M.map(this.mask, this.generator() < this.p.rate);
      this.values = M.multiply(inputs, this.mask);
    } else {
      this.values = inputs;
    }

    this.next_layer.feedForward(this.values, training);
  }

  train(inputs, target) {
    let in_errors = this.next_layer.train(this.values, target);

    this.errors = M.multiply(in_errors, this.mask);
    
    return this.errors;
  }

}
