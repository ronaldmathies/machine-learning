"use strict";

class Layer {

  constructor(label) {
    this.label = A.isNotNull("label", label);
  }

  compile(next_layer) {
	  // console.log("{}: Compiling layer", this.label)
  }

  feedForward(inputs) {
	  // console.log("{}: Feed forward", this.label)
  }

  train(inputs, targets) {
  }

}
