"use strict";

class AF {

  static none() {
    return {
      func: (i) => i,
      dfunc: (i) => i,
      useOriginalValues: false
    }
  }

  // Range (0, 1)
  static sigmoid() {
    let sigmoid = (v) => 1.0 / (1.0 + Math.exp(-v));
    return {//1.0 / (1.0 + exp(-1.0 * x));
      func: (i) => M.map(i, (_, v) => sigmoid(v)),
      dfunc: (i) => M.map(i, (_, v) => v * (1.0 - v)),
      useOriginalValues: false
    }
  }

  static sigmoid2() {
    let sigmoid = (v) => 1.0 / (1.0 + Math.exp(-v));
    return {
      func: (i) => M.map(i, (_, v) => sigmoid(v)),
      dfunc: (i) => M.map(i, (_, v) => sigmoid(v) * (1.0 - sigmoid(v))),
      useOriginalValues: false
    }
  }

  // Range (-1, 1)
  static tanh() {
    return {
      func: (i) => M.map(i, (_, v) => Math.tanh(v)),
      dfunc: (i) => M.map(i, (_, v) => 1.0 - (v * v)),
      useOriginalValues: false
    }
  }

  // Range (-PI/2, PI/2)
  static arctan() {
    return {
      func: (i) => M.map(i, (_, v) => Math.atan(v)),
      dfunc: (i) => M.map(i, (_, v) => 1.0 / ((v * v) + 1.0)),
      useOriginalValues: true
    }
  }

  // Range (-1, 1)
  static softsign() {
    return {
      func: (i) => M.map(i, (_, v) => v / (1.0 + Math.abs(v))),
      dfunc: (i) => M.map(i, (_, v) => 1.0 / Math.pow((Math.abs(v) + 1.0), 2.0)),
      useOriginalValues: true
    }
  }

  // Range [0, INFINITY)
  static relu() {
    return {
      func: (i) => M.map(i, (_, v) => v < 0.0 ? 0.0 : v),
      dfunc: (i) => M.map(i, (_, v) => v < 0.0 ? 0.0 : 1.0),
      useOriginalValues: false
    }
  }

  // Range [0, INFINITY)
  static elu() {
    return {
      func: (i) => M.map(i, (_, v) => v >= 0.0 ? v : Math.exp(v) - 1.0),
      dfunc: (i) => M.map(i, (_, v) => v >= 0.0 ? 1.0 : Math.exp(v)),
      useOriginalValues: false
    }
  }

  // Range (-INFINITY, INFINITY)
  static leaky_relu() {
    return {
      func: (i) => M.map(i, (_, v) => v < 0.0 ? 0.01 * v : v),
      dfunc: (i) => M.map(i, (_, v) => v < 0.0 ? 0.01 : 1.0),
      useOriginalValues: false
    }
  }

  // Range (0, INFINITY)
  static softplus() {
    return {
      func: (i) => M.map(i, (_, v) => Math.log(1.0 + Math.exp(v))),
      dfunc: (i) => M.map(i, (_, v) => 1.0 / (1.0 + Math.exp(-v))),
      useOriginalValues: true
    }
  }

  // Range (0, 1]
  static gaussian() {
    return {
      func: (i) => M.map(i, (_, v) => Math.exp(-1.0 * (v * v))),
      dfunc: (i) => M.map(i, (_, v) => -2.0 * v * Math.exp(-1.0 * (v * v))),
      useOriginalValues: true
    }
  }

  // Range (0, 1)
  static softmax() {
    return {
      func: (i) => {
        let sum = 0;
        M.loop(i, (_, v) => sum += Math.exp(v));
        return M.map(i, (_, v) => Math.exp(v) / sum);
      },
      dfunc: (i) => console.log("Not Implemented."),
      useOriginalValues: false
    }
  }

}
