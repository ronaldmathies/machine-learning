"use strict";

let reshapeDefaultConfig = {
  defaults: {
    input_shape: { d: 0, r: 0, c: 0 },
    target_shape: { d: 0, r: 0, c: 0 }
  },

  get: function(target, name) {
    return target.hasOwnProperty(name) ? target[name] : reshapeDefaultConfig.defaults[name];
  }
};

class ReshapeLayer extends Layer {

  constructor(config) {
    super("RL");

    this.p = new Proxy(config == null ? {} : config, reshapeDefaultConfig);

    A.isLargerThenOrUndefined("input_shape.d", this.p.input_shape.d, -1);
    A.isLargerThenOrUndefined("input_shape.r", this.p.input_shape.r, 0);
    A.isLargerThen("input_shape.c", this.p.input_shape.c, 0);
    A.isLargerThenOrUndefined("target_shape.d", this.p.target_shape.d, -1);
    A.isLargerThenOrUndefined("target_shape.r", this.p.target_shape.r, 0);
    A.isLargerThen("target_shape.c", this.p.target_shape.c, 0);

    this.values = [];
  }

  compile(next_layer) {
    super.compile();

    this.next_layer = A.isNotNull("next_layer", next_layer);
  }

  feedForward(inputs, training = false) {
    super.feedForward();

    this.values = M.reshape(inputs, this.p.target_shape);

    this.next_layer.feedForward(this.values, training);
  }

  train(inputs, target) {
    let in_errors = this.next_layer.train(this.values, target);

    this.errors = M.reshape(in_errors, this.p.input_shape);
    
    return this.errors;
  }

}
