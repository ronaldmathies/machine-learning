"use strict";

let denseDefaultConfig = {
  defaults: {
    units: 1.0,
    input_shape: {r: 1.0, c: 1.0},
    activation_function: AF.sigmoid(),
    learning_rate: 0.1,
    bias_initializer: IF.random(),
    kernel_initializer: IF.random()
  },

  get: function(target, name) {
    return target.hasOwnProperty(name) ? target[name] : denseDefaultConfig.defaults[name];
  }
};

class DenseLayer extends Layer {

  constructor(config) {
    super("HL");

    this.p = new Proxy(config == null ? {} : config, denseDefaultConfig);

    A.isLargerThen("units", this.p.units, 0);
    A.isLargerThen("input_shape.r", this.p.input_shape.r, 0);
    A.isLargerThen("input_shape.c", this.p.input_shape.c, 0);
    A.isLargerThen("learning_rate", this.p.learning_rate, 0);
    A.isNotNull("activation_function", this.p.activation_function);
    A.isFunction("bias_initializer", this.p.bias_initializer);
    A.isFunction("kernel_initializer", this.p.kernel_initializer);

    this.values = null;
    this.values_activated = null;
    this.errors = null;
  }

  compile(next_layer) {
    super.compile();

    this.next_layer = A.isNotNull("next_layer", next_layer);

    this.bias = M.map(M.createArray({r: this.p.units, c: 1}), this.p.bias_initializer);
    this.weights = M.map(M.createArray({r: this.p.units, c: this.p.input_shape.r}), this.p.kernel_initializer);

    // this.p.output_shape = {r: this.p.units c: this.p.input_shape.c};
  }

  feedForward(input, training = false) {
    super.feedForward();

    // A.isDimensionEqualTo("input", input, this.p.input_shape);

    this.values = M.add(M.dot2D(this.weights, input), this.bias);
    this.values_activated = this.p.activation_function.func(this.values);

    this.next_layer.feedForward(this.values_activated, training);
  }

  train(input, target) {
    // Then train the next layer first.
    let in_errors = this.next_layer.train(this.values_activated, target);

    // Calculate the gradient.
    let values = this.p.activation_function.useOriginalValues ? this.values : this.values_activated;
    let gradients = M.multiply(M.multiply(this.p.activation_function.dfunc(values), in_errors), this.p.learning_rate);

    // Calculate input -> hidden deltas
    // hidden_gradients * inputsT
    let weights_deltas = M.dot2D(gradients, M.transpose(input));

    // Adjust the input -> hidden deltas
    M.add(this.weights, weights_deltas);

    // Adjust the hidden bias.
    M.add(this.bias, gradients);

    this.errors = M.dot2D(M.transpose(this.weights), in_errors);
    return this.errors;
  }

}
