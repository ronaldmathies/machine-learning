"use strict";

// filters: Integer, the dimensionality of the output
//          space (i.e. the number of output filters in the convolution).
// kernel:  Two integers, specifying the width and height of
//          the 2D convolution window.
// strides: An integer or tuple/list of 2 integers, specifying the strides of
//          the convolution along the width and height.
// padding: one of "valid" or "same"
let clDefaultConfig = {
  defaults: {
    input_shape: { d: 0, r: 0, c: 0 },
    kernel: { d: 0, r: 0, c: 0 },
    stride: { r: 1, c: 1 },
    padding: "valid",
    kernel_initializer: IF.random()
  },

  get: function(target, name) {
    return target.hasOwnProperty(name) ? target[name] : clDefaultConfig.defaults[name];
  }
};

/*
 * More documentation about convolutional layers can be found at:
 *
 *
 * FEATURES TO IMPLEMENT:
 *
 * 0. Backpropagation...
 * 1. Handle multiple input arrays (channels?)
 * 4. Activation function?
 * 5. Learning rate (hyper parameter)
 *
 * Feed forward:
 * https://ujjwalkarn.me/2016/08/11/intuitive-explanation-convnets/
 * https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/convolution.html
 *
 * Back propagation:
 * https://medium.com/@2017csm1006/forward-and-backpropagation-in-convolutional-neural-network-4dfa96d7b37e
 * https://becominghuman.ai/only-numpy-implementing-convolutional-neural-network-using-numpy-deriving-forward-feed-and-back-458a5250d6e4
 *
 * Visualization of convolutional / pooling layers:
 * http://scs.ryerson.ca/~aharley/vis/conv/flat.html
 */
class ConvolutionalLayer extends Layer {

  constructor(config) {
    super("CL");

    this.p = new Proxy(config == null ? {} : config, clDefaultConfig);

    A.isLargerThen("input_shape.d", this.p.input_shape.d, 0);
    A.isLargerThen("input_shape.r", this.p.input_shape.r, 0);
    A.isLargerThen("input_shape.c", this.p.input_shape.c, 0);
    A.isLargerThen("kernel.d", this.p.kernel.d, 0);
    A.isLargerThen("kernel.r", this.p.kernel.r, 0);
    A.isLargerThen("kernel.c", this.p.kernel.c, 0);
    A.isLargerThen("stride.r", this.p.stride.r, 0);
    A.isLargerThen("stride.c", this.p.stride.c, 0);
    A.isAnyOf("padding", this.p.padding, ["valid", "same"]);
    A.isFunction("kernel_initializer", this.p.kernel_initializer);

    this.padding_shape = this.calculatePaddingShape();
    this.output_shape = this.calculateOutputShape();

    this.values = M.createArray(this.output_shape, true);
    this.weights = M.map(M.createArray(this.p.kernel), this.p.kernel_initializer);
  }

  compile(next_layer) {
    super.compile();

    this.next_layer = A.isNotNull("next_layer", next_layer);
  }

  feedForward(inputs, training = false) {
    super.feedForward();

    let kernels = new Array(this.p.kernel.d);
    for (let d = 0, _d = this.weights.dim().d; d < _d; d++) {
      kernels[d] = M.flip(this.weights[d]);
    }

    let values_idx = 0;
    for (let input_idx = 0; input_idx < this.p.input_shape.d; input_idx++) {
      this.processConvolutionsOn(inputs[input_idx], values_idx, this.values, kernels);
    }

    this.next_layer.feedForward(this.values, training);
  }

  processConvolutionsOn(input, values_idx, values, kernels) {
    if (this.p.padding === 'same') {
      input = M.pad2D(input, this.padding_shape);
    }

    kernels.forEach((kernel, k_idx) => {
      this.processConvolutionOn(input, values[values_idx + k_idx], kernel);
    });

    values_idx += this.p.kernel.d;
  }

  processConvolutionOn(input, value, kernel) {
    M.loop(value, (v_pos) => {
      let value_r = v_pos.r * this.p.stride.r;
      let value_c = v_pos.c * this.p.stride.c;

      let perceptive_field = M.sub2D(input, value_r, this.p.kernel.r, value_c, this.p.kernel.c);

      let sum = 0;
      M.loop(kernel, (k_pos, k_v) => {
        sum += perceptive_field.get(k_pos) * k_v;
      });

      value.set(v_pos, sum);
    });
  }

  train(inputs, targets) {
    let in_errors = this.next_layer.train(this.values, targets);

    // ????????
    // ????????

    return this.errors;
  }

  calculatePaddingShape() {
    if (this.p.padding === 'same') {
      let output_shape = {
        d: 0,
        r: Math.ceil(this.p.input_shape.r / this.p.stride.r),
        c: Math.ceil(this.p.input_shape.c / this.p.stride.c)
      };

      let padding_h = ((output_shape.r - 1) * this.p.stride.r) + this.p.kernel.r - this.p.input_shape.r;
      let padding_v = ((output_shape.c - 1) * this.p.stride.c) + this.p.kernel.c - this.p.input_shape.c;

      let padding_t = Math.floor(padding_v / 2);
      let padding_b = padding_v - padding_t;
      let padding_l = Math.floor(padding_h / 2);
      let padding_r = padding_h - padding_l;

      return { t: padding_t, r: padding_r, b: padding_b, l: padding_l, };
    }

    return { t: 0, r: 0, b: 0, l: 0 };
  }

  calculateOutputShape() {
    // 2 * p removed since we calculate padding as a total on the horizontal line and vertical line.
    let r = ((this.p.input_shape.r - this.p.kernel.r + (this.padding_shape.t + this.padding_shape.b)) / this.p.stride.r) + 1.0;
    if (r % 1 != 0) {
      throw "Combination of input.r, kernel.r size and stride.r doesn't add up to an integer for the row ( W - K + ( 2 * P ) / S) + 1 = " + r;
    }

    let c = ((this.p.input_shape.c - this.p.kernel.c + (this.padding_shape.l + this.padding_shape.r)) / this.p.stride.c) + 1.0;
    if (c % 1 != 0) {
      throw "Combination of input.c, kernel.c size and stride.c doesn't add up to an integer for the col ( W - K + ( 2 * P ) / S) + 1" + c;
    }

    return { d: this.p.kernel.d * this.p.input_shape.d, r: r, c: c };
  }



}
