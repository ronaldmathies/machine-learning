"use strict";

class PoolFunction {

  constructor(func, dfunc) {
    this.func = func;
    this.dfunc = dfunc;
  }

}

let maxPool = new PoolFunction(
  (matrix) => {
    return matrix.max();
  },
  (matrix) => {
    return matrix.max();
  }
);

let minPool = new PoolFunction(
  (matrix) => {
    return M.min(matrix);
  },
  (matrix) => {
    return M.min(matrix);
  }
);

let avgPool = new PoolFunction(
  (matrix) => {
    return M.avg2D(matrix);
  },
  () => {
    throw ("Average pool derivative function not implemented yet.");
  }
);

let pDefaultConfig = {
  defaults: {
    input_shape: { d: 0, r: 0, c: 0 },
    kernel: { r: 2, c: 2 },
    pool_func: maxPool,
    stride: { r: 1, c: 1 },
    padding: "valid"
  },

  get: function(target, name) {
    return target.hasOwnProperty(name) ? target[name] : pDefaultConfig.defaults[name];
  }
};

/**
 * Focus:
 * https://wiseodd.github.io/techblog/2016/07/18/convnet-maxpool-layer/
 * https://deepnotes.io/convlayer
 *
 * https://github.com/karpathy/convnetjs/blob/master/src/convnet_layers_pool.js
 *
 * Source of information:
 * http://machinelearninguru.com/computer_vision/basics/convolution/convolution_layer.html
 * http://machinelearninguru.com/computer_vision/basics/convolution/image_convolution_1.html
 * http://cs231n.github.io/convolutional-networks/
 * https://adeshpande3.github.io/A-Beginner%27s-Guide-To-Understanding-Convolutional-Neural-Networks-Part-2/
 */
class PoolLayer extends Layer {

  constructor(config) {
    super("PL");

    this.p = new Proxy(config == null ? {} : config, pDefaultConfig);

    A.isLargerThenOrUndefined("input_shape.d", this.p.input_shape.d, 0);
    A.isLargerThen("input_shape.r", this.p.input_shape.r, 0);
    A.isLargerThen("input_shape.c", this.p.input_shape.c, 0);
    A.isLargerThen("kernel.r", this.p.kernel.r, 0);
    A.isLargerThen("kernel.c", this.p.kernel.c, 0);
    A.isLargerThen("stride.r", this.p.stride.r, 0);
    A.isLargerThen("stride.c", this.p.stride.c, 0);
    A.isOfType("pool_func", this.p.pool_func, PoolFunction);
    A.isAnyOf("padding", this.p.padding, ["valid", "same"]);

    this.padding_shape = this.calculatePaddingShape();
    this.output_shape = this.calculateOutputShape();

    this.values = M.createArray(this.output_shape);

    // The pool mask is not filled with single digits but with
    // coordinates from where the original max value was
    // taken from.
    this.pool_masks = M.createArray(this.output_shape);
  }

  compile(next_layer) {
    super.compile();

    this.next_layer = A.isNotNull("next_layer", next_layer);
  }

  feedForward(inputs, training = false) {
    super.feedForward();

    // Can use subclassing for this.
    if (M.isDim3D(this.p.input_shape)) {
      for (let input_idx = 0; input_idx < inputs.length; input_idx++) {
        this.processPoolingOn(inputs[input_idx], this.values[input_idx], this.pool_masks[input_idx]);
      }
    } else {
      this.processPoolingOn(inputs, this.values, this.pool_masks);
    }

    this.next_layer.feedForward(this.values, training);
  }

  // Change implementation on specific subclass.
  processPoolingOn(input, values, pool_mask) {
    // Apply padding on the input.
    if (this.p.padding === 'same') {
      input = M.pad2D(input, this.padding_shape);
    }

    M.loop(values, (pos) => {
      let value_r = pos.r * this.p.stride.r;
      let value_c = pos.c * this.p.stride.c;

      // Resolve the perceptive field on wich to apply the pooling function.
      let perceptive_field = M.sub2D(input, value_r, this.p.kernel.r, value_c, this.p.kernel.c);

      // Apply the pooling function on the pereptive field.
      let pool_result = this.p.pool_func.func(perceptive_field);

      values.set(pos, pool_result.v);
      pool_mask.set(pos, {
        r: value_r + pool_result.r - this.padding_shape.t,
        c: value_c + pool_result.c - this.padding_shape.l
      });
    });
  }

  processMaskOn(in_errors, errors, pool_mask) {
    M.loop(in_errors, (pos, v) => {
      let m_pos = pool_mask.get(pos);
      errors.set(m_pos, errors.get(m_pos) + v);
    });
  }

  train(inputs, targets) {
    let in_errors = this.next_layer.train(this.values, targets);

    this.errors = M.createArray(this.p.input_shape, true);
    if (M.isDim3D(this.p.input_shape)) {
      for (let error_idx = 0; error_idx < in_errors.length; error_idx++) {
        this.processMaskOn(in_errors[error_idx], this.errors[error_idx], this.pool_masks[error_idx]);
      }
    } else {
      this.processMaskOn(in_errors, this.errors, this.pool_masks);
    }

    return this.errors;
  }

  // Can be optimised in subclassing (especially for 1D)
  calculatePaddingShape() {
    if (this.p.padding === 'same') {
      let output_shape = {
        d: 0,
        r: Math.ceil(this.p.input_shape.r / this.p.stride.r),
        c: Math.ceil(this.p.input_shape.c / this.p.stride.c)
      };

      let padding_h = ((output_shape.r - 1) * this.p.stride.r) + this.p.kernel.r - this.p.input_shape.r;
      let padding_v = ((output_shape.c - 1) * this.p.stride.c) + this.p.kernel.c - this.p.input_shape.c;

      let padding_t = Math.floor(padding_v / 2);
      let padding_b = padding_v - padding_t;
      let padding_l = Math.floor(padding_h / 2);
      let padding_r = padding_h - padding_l;

      return { t: padding_t, r: padding_r, b: padding_b, l: padding_l, };
    }

    return { t: 0, r: 0, b: 0, l: 0 };
  }

  // Can be optimised in subclassing (especially for 1D)
  calculateOutputShape() {
    // 2 * p removed since we calculate padding as a total on the horizontal line and vertical line.
    let r = ((this.p.input_shape.r - this.p.kernel.r + (this.padding_shape.t + this.padding_shape.b)) / this.p.stride.r) + 1.0;
    if (r % 1 != 0) {
      throw "Combination of input.r, kernel.r size and stride.r doesn't add up to an integer for the row ( W - K + ( 2 * P ) / S) + 1 = " + r;
    }

    let c = ((this.p.input_shape.c - this.p.kernel.c + (this.padding_shape.l + this.padding_shape.r)) / this.p.stride.c) + 1.0;
    if (c % 1 != 0) {
      throw "Combination of input.c, kernel.c size and stride.c doesn't add up to an integer for the col ( W - K + ( 2 * P ) / S) + 1" + c;
    }

    if (M.isDim3D(this.p.input_shape)) {
      return { d: this.p.input_shape.d, r: r, c: c };
    } else {
      return { r: r, c: c };
    }
  }

}
