if (!Array.prototype.get) {
  Array.prototype.get = function(dim) {
    if (this.is1d()) {
      return this[dim.c];
    } else if (this.is2d()) {
      return this[dim.r][dim.c];
    } else if (this.is3d()) {
      return this[dim.d][dim.r][dim.c];
    }
  };
} else {
  throw ("Get method already exists.");
}

if (!Array.prototype.set) {
  Array.prototype.set = function(dim, v) {
    if (this.is1d()) {
      this[dim.c] = v;
    } else if (this.is2d()) {
      this[dim.r][dim.c] = v;
    } else if (this.is3d()) {
      this[dim.d][dim.r][dim.c] = v;
    }
  }
} else {
  throw ("Set method already exists.");
}

if (!Array.prototype.is1d) {
  Array.prototype.is1d = function() {
    return !(this[0] instanceof Array);
  }
} else {
  throw ("is2d method already exists.")
}

if (!Array.prototype.is2d) {
  Array.prototype.is2d = function() {
    return this[0] instanceof Array && !(this[0][0] instanceof Array);
  }
} else {
  throw ("is2d method already exists.")
}

if (!Array.prototype.is3d) {
  Array.prototype.is3d = function() {
    return this[0] instanceof Array && this[0][0] instanceof Array;
  }
} else {
  throw ("is3d method already exists.")
}

if (!Array.prototype.dim) {
  Array.prototype.dim = function() {
    if (this.is1d()) {
      return { c: this.length };
    } else if (this.is2d()) {
      return { r: this.length, c: this[0].length };
    } else if (this.is3d()) {
      return { d: this.length, r: this[0].length, c: this[0][0].length };
    }
  }
} else {
  throw ("Dim method already exists.")
}

if (!Array.prototype.max) {
  Array.prototype.max = function() {
    let r = { v: Number.MIN_VALUE };
    M.loop(this, (pos, v) => {
      if (Math.max(r.v, v) == v) {
        pos.v = v;
        r = pos;
      }
    });

    return r;
  }
} else {
  throw ("Max method already exists.")
}

if (!Array.prototype.min) {
  Array.prototype.min = function() {
    let r = { v: Number.MAX_VALUE };
    M.loop(this, (pos, v) => {
      if (Math.min(r.v, v) == v) {
        pos.v = v;
        r = pos;
      }
    });

    return r;
  }
}

if (!Array.prototype.sum) {
  Array.prototype.min = function() {
    let sum = 0.0;
    M.loop(this, (_, v) => sum += v);
    return sum;
  }
}
