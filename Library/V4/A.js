"use strict";

class A {

  static isDimEqual(name1, dim1, name2, dim2) {
    if (NNConfig.disable_validations) return;

    if (dim1.d != dim2.d || dim1.r != dim2.r || dim1.c != dim2.c) {
      throw ("Depth, columns and/or rows of " + name1 + " do not match " + name2 + ".");
    }
  }

  static isAnyOf(name, value, valid_values) {
    if (NNConfig.disable_validations) return value;

    A.isNotNull(name, value);

    if (valid_values.indexOf(value) == -1) {
      throw ("The field " + name + " should be any of the following values " + valid_values + ".");
    }

    return value;
  }

  static isFunction(name, fn) {
    if (NNConfig.disable_validations) return fn;

    A.isNotNull(name, fn);
    if (!(fn && {}.toString.call(fn) === '[object Function]')) {
      throw ("The field " + name + " is not a function.");
    }

    return fn;
  }

  static isAnyOfType(name, value, valid_types) {
    if (NNConfig.disable_validations) return value;

    A.isNotNull(name, value);

    let isValid = false;
    for (let key in valid_types) {
      if (value instanceof valid_types[key]) {
        isValid = true;
      }
    }

    if (!isValid) {
      throw ("The field " + name + " should be any of the following types " + valid_types.map((type) => type.name) + ".");
    }

    return value;
  }

  static isOfType(name, value, type) {
    if (NNConfig.disable_validations) return value;

    A.isNotNull(name, value);
    if (!(value instanceof type)) {
      throw ("The field " + name + " is not of type " + type.name() + ".");
    }

    return value;
  }

  static isLargerThenOrUndefined(name, value, min_value) {
    if (NNConfig.disable_validations) return value;

    if (typeof value === "undefined") {
      return value;
    }

    A.isLargerThen(name, value, min_value);
  }

  static isLargerThen(name, value, min_value) {
    if (NNConfig.disable_validations) return value;

    A.isNumber(name, value);
    if (!(value > min_value)) {
      throw ("The field " + name + " should be larger then " + min_value + ".");
    }

    return value;
  }

  static isLargerThenOrEqualTo(name, value, min_value) {
    if (NNConfig.disable_validations) return value;

    A.isNumber(name, value);
    if (!(value >= min_value)) {
      throw ("The field " + name + " should be larger or equal to " + min_value + ".");
    }

    return value;
  }

  static isSmallerThen(name, value, max_value) {
    if (NNConfig.disable_validations) return value;

    A.isNumber(name, value);
    if (!(value < max_value)) {
      throw ("The field " + name + " should be smaller then " + max_value + ".");
    }

    return value;
  }

  static isEqualTo(name, value, equal_value) {
    if (NNConfig.disable_validations) return value;

    A.isNumber(name, value);
    if (value !== equal_value) {
      throw ("The field " + name + " should be equal to " + equal_value + ".");
    }

    return value;
  }

  static isSmallerThenOrEqualTo(name, value, max_value) {
    if (NNConfig.disable_validations) return value;

    A.isNumber(name, value);
    if (!(value <= max_value)) {
      throw ("The field " + name + " should be smaller or equal to " + max_value + ".");
    }

    return value;
  }

  static isNumber(name, value) {
    if (NNConfig.disable_validations) return value;

    if (isNaN(parseFloat(value)) && isNaN(value - 0)) {
      throw ("The field " + name + " is not a number.");
    }

    return value;
  }

  static isNotNull(name, value) {
    if (NNConfig.disable_validations) return value;

    if (value == null) {
      throw ("The field " + name + " must not be null.");
    }

    return value;
  }

  static isDimensionEqualTo(name, input, dim2) {
    if (NNConfig.disable_validations) return input;

    let dim1 = M.dim2D(A.isNotNull(name, input));
    if (dim1.r !== dim2.r || dim1.c !== dim2.c) {
      throw ("The field " + name + " with dimensions R:" + dim1.r + ", C:" + dim1.c + " is not equal to R:" + dim2.r + ", C:" + dim2.c + ".");
    }

    return input;
  }

}
