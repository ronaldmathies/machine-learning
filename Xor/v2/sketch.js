let training_data = [
	{ inputs: [1, 0], targets: [1] },
	{ inputs: [0, 1], targets: [1] },
	{ inputs: [1, 1], targets: [0] },
	{ inputs: [0, 0], targets: [0] }
];

let nn;

function setup() {
  nn = new NeuralNetwork(2, [4], 1);
	nn.setLearningRate(0.2);
	nn.setActivationFunction(sigmoid);
	nn.compile();

  startTraining();

  console.log("1-0 = " + nn.predict([1, 0])[0]);
  console.log("0-1 = " + nn.predict([0, 1])[0]);
  console.log("1-1 = " + nn.predict([1, 1])[0]);
  console.log("0-0 = " + nn.predict([0, 0])[0]);
}

function startTraining() {
	console.time('Training');
	for (let times = 0; times < 20000; times++) {
	  let data = random(training_data);
	  nn.train(data.inputs, data.targets);
  }
	console.timeEnd('Training');
}

function draw() {
}
