let training_data = [
  { inputs: [
      [1.0],
      [0.0]
    ], targets: [
      [1.0]
    ] },
  { inputs: [
      [0.0],
      [1.0]
    ], targets: [
      [1.0]
    ] },
  { inputs: [
      [1.0],
      [1.0]
    ], targets: [
      [0.0]
    ] },
  { inputs: [
      [0.0],
      [0.0]
    ], targets: [
      [0.0]
    ] }
];

let nn;
let lr = 0.1;

function setup() {
  nn = new NeuralNetwork([
    new DenseLayer({ units: 4, input_shape: { d: 0, r: 2, c: 1 }, learning_rate: lr, activation_function: AF.sigmoid() }),
    new OutputLayer({ units: 1, input_shape: { d: 0, r: 4, c: 2 }, learning_rate: lr, activation_function: AF.sigmoid() })
  ]);
  nn.compile();

  startTraining();

  console.log("1-0 = " + nn.predict([
    [1],
    [0]
  ])[0]);
  console.log("0-1 = " + nn.predict([
    [0],
    [1]
  ])[0]);
  console.log("1-1 = " + nn.predict([
    [1],
    [1]
  ])[0]);
  console.log("0-0 = " + nn.predict([
    [0],
    [0]
  ])[0]);
}

function startTraining() {
  console.time('Training');
  for (let times = 0; times < 20000; times++) {
    let data = random(training_data);
    nn.train(data.inputs, data.targets);
  }
  console.timeEnd('Training');
}

function draw() {}
