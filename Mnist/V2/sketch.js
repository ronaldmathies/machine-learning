let nn;
let mnist;

function setup() {
  createCanvas(280, 280);
  background(255);

  // 93.25% (748, 64, 10), sigmoid roundedFilter on mnist data.
  // 93.41%  (748, 64, 10), sigmoid normalizedFilter on mnist data.

  nn = new NeuralNetwork(784, [64], 10);
  nn.setLearningRate(0.1);
  nn.setActivationFunction(sigmoid);
  nn.compile();

  select('#train').mousePressed(() => train());
  select('#test').mousePressed(() => startPrediction());
  select('#guess').mousePressed(() => startUserPrediction());
  select('#clear').mousePressed(() => background(255));

  mnist = new Mnist();
  mnist.loadTrainingData()
    .then(function() {
      console.log("Finished loading training and test data.");
    });
}

function train() {
  console.time('Training');

  for (let index = 0; index < 5000; index++) { //  mnist.train_images.length

  	if (index == 400) {
		console.log("400");
	}

    let result = [];
    for (let i = 0; i < 10; i++) {
      result.push(i == mnist.train_labels[index] ? 1 : 0);
    }

    nn.train(mnist.train_images[index], result);

    if (index % 1000 == 0) {
      console.log("Trained " + index + " times.");
    }
  }

  console.timeEnd('Training');
}

function startPrediction() {
  let correct = 0;
  for (let index = 0; index < mnist.test_images.length; index++) {
    let result = nn.predict(mnist.test_images[index]);
    let guessed = result.indexOf(max(result));

    if (mnist.test_labels[index] == guessed) {
      correct++;
    }

    if (index % 500 == 0) {
      console.log("Tested " + index + " times.");
    }

  }
  console.log(correct + " times correct.");
  console.log(((100 / mnist.test_images.length) * correct) + "% correct.");
}

function startUserPrediction() {
  let inputs = [];
  let img = get();
  img.resize(28, 28);
  img.loadPixels();
  for (let index = 0; index < 784; index++) {
    let bright = img.pixels[index * 4];
    inputs[index] = (255 - bright) / 255.0;
  }

  let guess = nn.predict(inputs);
  let m = max(guess);
  let classification = guess.indexOf(m);
  console.log("Is it the number: " + classification);
}

function draw() {
  strokeWeight(20);
  stroke(0);
  if (mouseIsPressed) {
    line(pmouseX, pmouseY, mouseX, mouseY);
  }
}


function displayinput(inputs) {
  let line = "";
  for (let y = 0; y < 28; y++) {
    for (let x = 0; x < 28; x++) {
      line += inputs[(y * 28) + x];
    }
    console.log(line);
    line = "";
  }
}
