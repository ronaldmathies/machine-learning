let nn;
let mnist;

function setup() {
  createCanvas(280, 280);
  background(255);

  //false = Training: 25880.02392578125ms
  NNConfig.disable_validations = true;

  // 93.25% (748, 64, 10), sigmoid roundedFilter on mnist data.
  // 93.41%  (748, 64, 10), sigmoid normalizedFilter on mnist data.
  nn = new NeuralNetwork([
    new DenseLayer({
      units: 64,
      input_shape: {
        r: 784, c: 1
      },
      learning_rate: 0.1,
      activation_function: AF.sigmoid(),
      bias_initializer: IF.random("seed1"),
      kernel_initializer: IF.random("seed2")
    }),
    new OutputLayer({
      units: 10,
      input_shape: {
        r: 64
      },
      learning_rate: 0.1,
      activation_function: AF.sigmoid(),
      bias_initializer: IF.random("seed3"),
      kernel_initializer: IF.random("seed4")
    })
  ]);
  nn.compile();

  select('#train').mousePressed(() => train());
  select('#test').mousePressed(() => startPrediction());
  select('#guess').mousePressed(() => startUserPrediction());
  select('#clear').mousePressed(() => background(255));

  mnist = new Mnist();
  mnist.loadTrainingData()
    .then(function() {
      console.log("Finished loading training and test data.");
    });
}

function train() {
  console.time('Training');

  for (let index = 0; index < 5000; index++) { // mnist.train_images.length
    let result = [];
    for (let i = 0; i < 10; i++) {
      result.push([i == mnist.train_labels[index] ? 1 : 0]);
    }

    let target = M.reshape(result, {
      d: 0,
      r: 10,
      c: 1
    });

    let input = M.reshape(mnist.train_images[index], {
      d: 0,
      r: 784,
      c: 1
    });
    nn.train(input, target);

    if (index % 1000 == 0) {
      console.log("Trained " + index + " times.");
    }
  }

  console.timeEnd('Training');
}

function startPrediction() {
  let correct = 0;
  for (let index = 0; index < mnist.test_images.length; index++) {

    let input = M.reshape(mnist.test_images[index], {
      d: 0,
      r: 784,
      c: 1
    });

    let result = M.reshape(nn.predict(input), {d: 0, r: 10, c: 1});
    let guessed = result.indexOf(max(result));

    if (mnist.test_labels[index] == guessed) {
      correct++;
    }

    if (index % 500 == 0) {
      console.log("Tested " + index + " times.");
    }

  }
  console.log(correct + " times correct.");
  console.log(((100 / mnist.test_images.length) * correct) + "% correct.");
}

function startUserPrediction() {
  let inputs = [];
  let img = get();
  img.resize(28, 28);
  img.loadPixels();
  for (let index = 0; index < 784; index++) {
    let bright = img.pixels[index * 4];
    inputs[index] = (255 - bright) / 255.0;
  }

  let guess = nn.predict(inputs);
  let m = max(guess);
  let classification = guess.indexOf(m);
  console.log("Is it the number: " + classification);
}

function draw() {
  strokeWeight(20);
  stroke(0);
  if (mouseIsPressed) {
    line(pmouseX, pmouseY, mouseX, mouseY);
  }
}


function displayinput(inputs) {
  let line = "";
  for (let y = 0; y < 28; y++) {
    for (let x = 0; x < 28; x++) {
      line += inputs[(y * 28) + x];
    }
    console.log(line);
    line = "";
  }
}
